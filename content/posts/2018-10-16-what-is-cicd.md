---
title: "CI/CD 뒷북 때리기 - Part1.개념편"
date: 2018-10-15T20:42:41+09:00
tags: ["jenkins", "pipeline", "ci/cd"]
categories: ["devops"]
---

Cloud Native 시대에 이제와 CI/CD 가 웬말이냐 하시겠지만, 저같은 비 개발자들에게 이런 기술들은 어디 세미나에서나 듣던 이야기 입니다. 사실은 원래 `CI/CD 란 무엇인가` 로 시작하려 했지만 너무 거창한 듯 해서 맘편하게 뒷북 때리기로 정했습니다. 뒷북 떄리기 시리즈의 첫번째, 개념잡기를 시작해 보겠습니다.

## 뜬금없는 타이밍

서두에서 말한것처럼 너무 뜬금없습니다. 그러나 평소 인프라 엔지니어로서 개발과 가까워 지려 하지만 아무래도 현업 코드는 직접 개발하지 않다보니 가까워질래야 가까워질 수가 없네요. 요즘 블로그에 올라가는 글들은 언제나 뒷북입니다. 저에겐 늘 스쳐만 지나가던 단어들이라 이제는 정리를 해보려 합니다. 백번 말로만 듣고 아 그거는 이거에요 가 아닌 스스로의 공부를 위해서기도 하고, 스터디에서 강제로 시켰기 때문이기도 하지만, 스스로 한다고 하기도 했기 때문이죠.

어쨌든 해야 합니다. ~~안그러면 누구한테 혼납니다.~~

## 간단하게 개념부터 

뭐든지 새롭게 시작할 땐, `What is` 가 최곱니다. 찾아 봅니다. 구글에 대충 던져만 놔도 관련 글들이 너무 많아서 실제 도입 사례나 셋업만 있지 뭔지 설명하는 글들은 이제는 찾기 어렵네요.
아래와 같은 단어들이 눈에 띕니다.

- Continuous Integratin
- CI/CD 파이프라인
- 지속적인 통합
- 지속적 통합 및 지속적 전달(CI/CD) 구축 사례
- 지속적 배포(Continuous Delivery)
- Travis-ci 를 이용한 Ci/CD 와 도커를 이용한 Jenkins...

음.. 대충 저런 의미인가 보네요. 뭔가를 `멈추지 않고`, 아마도 `중단없이 통합`을 하거나 `배포를 하거나 전달을 하는 것들을 의미하는 것` 같아 보이네요. 계속 찾아 봅니다. 

위키^[CI/CD, https://en.wikipedia.org/wiki/CI/CD]에서는 매우 깔끔합니다. 아래처럼 그냥 단순한 링크만 보여줄 뿐입니다. 

![wikipedia 에서의 CI/CD](../imgs/what-is-cicd/cicd_wiki.png)

위의 링크를 따라가 봅시다.

### CI, Continuous integration

첫번째는 CI 입니다. 직역하면, `지속적인 통합` 정도 되겠네요. 위키에서는 다음과 같이 설명하고 있습니다.

**In software engineering, continuous integration (CI) is the practice of merging all developer working copies to a shared mainline several times a day. Grady Booch first proposed the term CI in his 1991 method, although he did not advocate integrating several times a day. Extreme programming (XP) adopted the concept of CI and did advocate integrating more than once per day – perhaps as many as tens of times per day.^[Continuous integration, https://en.wikipedia.org/wiki/Continuous_integration]**

소프트웨어 엔지니어링에선, CI 는 모든 개발자들의 동작하는 코드들 혹은 작업 사본을 하루에도 몇번 씩 메인 라인으로 병합, 통합하기 위한 연습 혹은 관행을 의미한다. Grady Booch 1991년에 CI 라는 단어를 최초로 제안했지만, 하루에 여러번 통합을 지지하지는 않았다. 익스트림 프로그래밍은 CI 의 컨셉을 채용하고, 하루에 한번 이상 통합하는 것을 지지했다 - 아마도 하루에 수십번 보다 더

사실 내용은 가서 보시면 알겠지만, 뭐가 엄청 많습니다. 그 중에서도 보면 필요한 근본적인 이유나 역사등의 내용도 있지만 무엇보다 우리에게 필요한 건 이제 이게 뭔진 알겠는데, 어디에 쓰이냐겠죠. 다음과 같은 내용이 있습니다.

- Common practices^[Common practices, https://en.wikipedia.org/wiki/Continuous_integration#Common_practices]
  - Maintain a code repository
  - Automate the build
  - Make the build self-testing
  - Everyone commits to the baseline every day
  - Every commit (to baseline) should be built
  - Keep the build fast
  - Test in a clone of the production environment
  - Make it easy to get the lastest deliverables
  - Everyone can see the results of the latest build
  - Automate deployment

CI 를 하게 되면 위와 같은 훌륭한(?) 일들을 할 수 있나 봅니다. 영어라 무섭지만, 뭔가 `코드를 저장소에 관리`하고 `빌드`하고 `테스트`하고 `배포하는 일련의 과정을 도와주는 툴`로 보입니다.
일단 여기까지 하고 CI 는 넘어 가봅니다. 우리에게 필요한 건 개념이니까요.

### CD, Continuous delivery

다음은 CD 입니다. 이번엔 `지속적인 전달` 정도로 보면 되겠네요. CD 는 가끔 `Continuous deployment` 로도 쓰이는 것 같은데, 전달과 배포는 어느 정도 일맥상통하는 것으로 보이니 넘어갑니다... 가 아니라 쓰다 보니 서로 조금의 차이가 있습니다. 이건 아래에서 다시.

우선 위키에서의 설명을 봅시다.

**Continuous delivery (CD or CDE) is a software engineering approach in which teams produce software in short cycles, ensuring that the software can be reliably released at any time and, when releasing the software, doing so manually. It aims at building, testing, and releasing software with greater speed and frequency. The approach helps reduce the cost, time, and risk of delivering changes by allowing for more incremental updates to applications in production. A straightforward and repeatable deployment process is important for continuous delivery.**

CD 는 소프트웨어 엔지니어링의 접근 방법 중 하나로서, 짧은 주기로 소프트웨어 결과를 생산하고, 어떤 때라도 확실하게 안정적으로 소프트웨어를 출시할 수 있고, 소프트웨어를 릴리즈할 때 수동으로 할 수 있다.(읭? ~~갑자기 이게 무슨..~~) 이것은 빌딩, 테스팅, 엄청난 속도와 주기로 소프트웨어를 출시하는 것을 목표로 한다. 이러한 접근은 어플리케이션의 프러덕션 환경에서 비용, 시간, 몇몇의 증분 업데이트의 허용으로 인한 변경을 제공하는 것의 리스크를 줄이는 데 도움이 된다. 분명하고 반복적인 배포 프로세스는 CD 에서 중요하다.

제 번역이 좀 개똥같긴 하나, 문맥적인 의미를 이해함에 있어서는 부족하지 않을 듯 합니다. 위의 CI 와 뭔가 비슷~한 느낌의 단어로 보입니다. 어떤 차이가 있을까요? 차이를 구분짓는게 의미가 있는건지는 계속 보시죠. 그리고 한가지 더, 바로 아랫줄에 이런 내용이 있습니다.

**CD contrasts with continuous deployment, a similar approach in which software is also produced in short cycles but through automated deployments rather than manual ones.**

CD 는 `continuous deployment` 와 대조적이다. continuous delivery 와 비슷하게 짧은 주기를 가지고 소프트웨어를 제공하는 비슷한 접근방법이지만, 수동적인 방법이 아닌 자동화된 배포를 사용한다.

오호라. 제가 틀렸네요. 처음엔 둘 다 모두 맞는 것인줄 알았는데, `continuous deployment` 와 `continuous delivery` 는 서로 다른 개념으로 봐도 될 것 같습니다. 그래서 위에서 소프트웨어를 릴리즈할 때 수동으로 할 수 있다 라는 내용이 있었군요. 개념정리가 필요한 우리들에게 도움이 되는 정보들이 위키에 계속해서 있습니다. 아래가 바로 그 내용입니다.

#### Continuous delivery 와 DevOps 의 관계

**Continuous delivery and DevOps are similar in their meanings and are often conflated, but they are two different concepts. DevOps has a broader scope, and centers around the cultural change, specifically the collaboration of the various teams involved in software delivery (developers, operations, quality assurance, management, etc.), as well as automating the processes in software delivery. Continuous delivery, on the other hand, is an approach to automate the delivery aspect, and focuses on bringing together different processes and executing them more quickly and more frequently. Thus, DevOps can be a product of continuous delivery, and CD flows directly into DevOps.^[Relationship to DevOps, https://en.wikipedia.org/wiki/Continuous_delivery#Relationship_to_DevOps]**

Continuous delivery 와 DevOps 는 종종 의미가 비슷하기도 하고 종종 하나로 합쳐지기도 하지만 이 두가지는 서로 다른 컨셉이다. DevOps 는 소프트웨어 딜리버리를 자동화하는 프로세스 뿐만 아니라 문화적 변화, 특히나 소프트웨어 딜리버리(개발자들, 그리고 운영, QA, 관리, 기타)에 참여한 다양한 팀들의 협업을 중심으로 한 더 넓은 개념이다. 반면에 Continous delivery 는 딜리버리 측면에서의 자동화를 위한 접근방법이며, 서로 다른 프로세스간의 브릿징이나 그것들의 실행을 보다 빠르게 만드는 데 있다. 따라서, DevOps 는 Continuous delivery 의 결과가 될 수 있고, CD 는 직접적으로 DevOps 로 흘러간다.

`DevOps` 가 `문화적인 측면을 이야기` 한다면, `Continuous delivery` 는 보다 `소프트웨어 적인 방법론을 의미`하는 거라고 이해하면 될 것 같습니다. 

#### Continuous delivery 와 Continuous deployment 의 관계

**Continuous delivery is often confused with continuous deployment with some people using the two terms interchangeably. Most practitioners, however, distinguish between the two by labeling continuous delivery as the ability to deliver software that can be deployed at any time through manual releases while continuous deployment does the same but through automated deployments. According to Martin Fowler, in order to do continuous deployment one must be doing continuous delivery. Academic literature, however, explicitly differentiates the two approaches as one uses manual deployments while the other uses automated ones.^[Relationship to continuous deployment, https://en.wikipedia.org/wiki/Continuous_delivery#Relationship_to_continuous_deployment]**

Continuous delivery 는 종종 continuous deployment 를 바꾸어 사용하는 사람들로 인해 혼동된다. 그러나 대부분의 실무자들은 이 두가지를, continuous delivery 는 수동의 형태로 소프트웨어를 언제든지 배포할 수 있는 기능으로 본다면 같은 일을 continuous deployment 는 자동화된 배포를 통해 동일한 일을 하는 기능으로서 서로 구별한다. 마틴 파울러에 의하면, continuous deployment 를 하기 위해선 continuous delivery 를 해야 한다고 한다. 그러나 학술적으로는 하나는 자동화 또 다른 하나는 수동의 형태를 사용하는 것으로 두 가지의 접근방법을 명확하기 분류하기도 한다.

처음 위에서 설명한 내용과 크게 다른 것은 없나 보네요. `소프트웨어를 언제나 배포할 수 있는 형태로 자동화가 되어 있다면, continuous deployment` 라고 부를 수 있고, `수동으로 배포할 수 있다면 그건 continuous delivery` 라고 부를 수 있겠네요. 뭐니뭐니해도 자동화가 좋겠지만 자동화라는 이름은 상황에 따라 다르게 작용할 수도 있으니 뭐가 좋다 나쁘다 라고 이 시점에선 확언할 순 없을 것 같습니다. 많은 경험이 필요할 듯 하네요.

## CI 와 CD

이제 개념은 대충 알았으니, 이 두 가지를 함께 보도록 하겠습니다. 둘다 비슷~한 것 같긴 한데 뭐가 다른지 혹은 어떻게 비교할 수 있는지 그림을 좀 찾아 보겠습니다. 누구나 들으면 알만한 회사 혹은 벤더의 설명 혹은 블로그 글들을 찾아 따라가 봅시다. 

사실 글을 찾아 따라간 것은 아니고, 구글에서 이미지 검색을 하며 보기 좋은 것들을 골라 봤습니다. 그랬더니 아래 처럼 큰 규모의 회사들의 글들이 발견이 되더군요. 사실 모두 읽어보면 좋겠지만, 별로 추천하고 싶진 않습니다. 대부분이 개념적인 글도 있지만 상품 소개에 가까운 것들도 있습니다. ~~그래서 저도 안읽었어요.~~

제가 이 글을 작성하는 이유는 개념을 잡고자 하는 것이지 완벽한 이해를 기대하는 것은 아니기 때문이지요.

### Puppet

자동화 툴로 유명한 puppet 에서 ['Continuous Delivery vs. Continuous Deployment: What's the Diff?'](https://puppet.com/blog/continuous-delivery-vs-continuous-deployment-what-s-diff) 라는 제목으로 작성한 blog post 가 있습니다. 무려 2013년도 자료죠. 정말 뒷북이죠? 해당 포스트의 그림을 조금 가져와 보겠습니다.

![puppet 의 continuous diagram](../imgs/what-is-cicd/puppet_continuous_diagram.gif)

위 그림은 참고로 [Inspired by Yassal Sundman's blog post on Crisp's Blog.](http://blog.crisp.se/2013/02/05/yassalsundman/continuous-delivery-vs-continuous-deployment) 입니다. 잘난체하는 게 아니라 원문이 실제 그렇습니다. 이건 중요한게 아니니 그림을 다시 봅시다.

거의 다 똑같은데, 프러덕션 레벨에서 배포를 할 때 수동이냐 자동이냐의 차이로 볼 수 있겠네요. 위키에서 설명하던 것과 같네요. 참고로 여기엔 CI 가 없네요. 그림에서 제외된 것인지 혹은 CI 의 개념을 따로 나눈 것인지는 확실하지 않습니다.

### Atlassian

다음은, wiki/jira 등 협업툴로 유명한 Atlassian 의 [Continuous integration vs. continuous delivery vs. continuous deployment](https://www.atlassian.com/continuous-delivery/ci-vs-ci-vs-cd) 라는 글입니다.

![Atlassian 의 ci vs cd](../imgs/what-is-cicd/ci-vs-cd.png)

CI 가 나왔습니다. CI 는 누군가가 만든 코드를 Build 하고 Test 까지 완료하는 것을 의미하나 봅니다. 그리고 그뒤의 Continuous Delivery 와 Continuous Deployment 는 위에서 본 것과 같이 Automatic vs. Manual 의 차이이고요.

### Microsoft

이번엔 마이크로소프트의 pipeline configurations 와 관련된 글입니다. [Set up a CI/CD pipeline to run automated tests efficiently](https://blogs.msdn.microsoft.com/visualstudioalmrangers/2017/04/20/set-up-a-cicd-pipeline-to-run-automated-tests-efficiently/) 에서는 Multiple levels of testing 에 대해 다음의 그림을 보여주고 있습니다.

![MS 의 Pipeline configurations](../imgs/what-is-cicd/ms-pipeline.png)

Build Pipeline 과 Release Pipeline 이 나누어져 있는게 특징이군요. 개발자가 만든 코드에 대해 빌드, 테스트, 배포후 확인하는 등의 과정을 거쳐 문제가 없다면 Release 를 위한 단계를 시작하나 봅니다.

### Cisco

사실 이 블로그 글은 아예 보지도 않았습니다. 그림만 가져왔죠. ~~크게 안궁금해서요.~~ 다만 Cisco 에서 작성한 [Cloud Agnostic CI/CD Pipeline for DevOps – #3 Process Steps](https://gblogs.cisco.com/ch-tech/how-to-devops-cloud-agnostic-cicd-pipeline-process-steps/) 이 포스팅에서는 CI/CD BluePrint Steps 라는 그림은 볼 만 합니다.

![Cisco 의 CI/CD BluePrint Steps](../imgs/what-is-cicd/cisco-cicd.png)

아마도 Cisco 의 장비를 AWS 와 연계하여 CI/CD 환경을 셋업하는 것처럼 보이네요. 어떤 내용인지는 잘 모르겠으나 CI 를 Load-Balancer 를 사용하기도 하는 군요. ~~그래도 안궁금해서 안읽어 볼겁니다.~~

### AWS

나올때가 되었죠? AWS 입니다. [AWS에서 CI/CD 파이프라인 설정](https://aws.amazon.com/ko/getting-started/projects/set-up-ci-cd-pipeline/)이라는 무려 한글 자료. 별 내용은 없어도 AWS 에서 제공하는 CodePipeline 과 관련된 서비스의 간단한 내용을 볼 수 있습니다. 해당 링크에는 구현 안내서도 제공하고 있고요. 아직 그런걸 볼 때는 아니니 우린 그림만 봅시다.

![AWS 의 CodePipeline](../imgs/what-is-cicd/aws-codepipeline.png)

Source / Deploy(Beta) / Deploy(Production) 으로 3가지 Stage 를 나누어 설명한게 인상적입니다.

### SemaphoreCI

어딘진 모르겠는데, 이런 자료도 있습니다. 이름도 거창한 Semaphoreci 의 [What's the Difference Between Continuous Integration, Continuous Deployment and Continuous Delivery?](https://semaphoreci.com/blog/2017/07/27/what-is-the-difference-between-continuous-integration-continuous-deployment-and-continuous-delivery.html) 라는 포스팅에서 가져온 그림입니다.

![Semaphore 의 CI 자료](../imgs/what-is-cicd/cicd-flow-dde970bb.jpg)

Source Version Control 시스템으로 개발자가 코드를 밀어 넣으면 자동으로 Built, Staging, Production 으로 지속적으로 배포가 되는 것 같아 보입니다. 

이쯤되면 느낌이 슬슬 와야할 것 같습니다. CI/CD 가 뭐고 뭘 위한 건지. 위의 그림들은 비슷하면서도 서로 다르죠? 아마도 회사의 CI/CD 전략 혹은 정책에 따라 만들기 나름일 것 같은데서 오는 이유일 듯 합니다. 계속 보시죠.

### Dzone 

CI/CD 와 관련된 자료를 찾다보니 계속해서 나오는 사이트가 있는데, 바로 Dzone.com 이라는 곳입니다. [AWS Velocity Series: CI/CD Pipeline as Code](https://dzone.com/articles/aws-velocity-series-cicd-pipeline-as-code) 에서 가져온 그림입니다.

![Dzone 의 CI/CD Pipeline as Code](../imgs/what-is-cicd/aws-velocity-ci-cd.png)

CI/CD 는 저기에 색으로 뚜렷히 보이네요.

여기선 CI/CD pipeline 이 단계별로 해야 하는 작업들에 대한 설명도 있습니다. 유용해서 덧붙여 봅니다. 영어지만 우리 겁내지 말기로 해요.

The CI/CD pipeline does the following:

1. Runs on every commit into your repository.
2. Updates itself if needed.
3. Builds the source code.
4. Executes unit tests.
5. Packages the application as an artifact.
6. Creates or updates the acceptance environment if needed.
7. Deploys the artifact into the acceptance environment.
8. Executes acceptance tests against the acceptance environment.
9. Creates or updates the production environment if needed.
10. Deploys the artifact into the production environment.

참 좋은 내용입니다.

또 Dzone 자료 입니다. [Stages of the Continuous Delivery Pipeline](https://dzone.com/articles/what-is-continuous-delivery-pipeline) 이라는 자료의 그림이에요.

![Dzone 의 CD Model](../imgs/what-is-cicd/cdmaturitymodel.png)

L1/L2/L3/L4 가 뭔지는 궁금하지만 이해하는덴 별 문제는 아니니 신경쓰지 말기로 하고 본다면, L1 packaging 는 코드레벨에서의 동작으로 보이네요. 그 후 색에 따라 Ci, CD 로 나누어 이해하면 될 것 같습니다.

2016 년의 자료이지만, CD pipeline 의 경우 보통 아래의 단계들로 구성되어 있다고 합니다.

1. Test Automation and CI
  - 새로운 기능, 혹은 프로토타입이 준비되고 baseline 에 해당 코드가 통합되는 스테이지
  - code 의 health status 가 nofity 되는 가장 효과적인 피드백 시스템 단계
  - Unit Test 와 Packaging 이 발생
2. Build Automation
  - Docker image 와 같은 개발자의 artifacts 를 Docker hub 또는 Amazon ECR 등의 registry 로 push 하는 단계
3. Alpha Daployments
  - 개발자의 변경분을 확인하는 단계
  - 다른 코드들과의 연계동작을 확인
4. Beta Dployments
  - 테스터들이 수동으로 테스팅하는 스테이지
  - CI 도구와는 별도로 완벽을 확인하려면 수동 테스트가 필요
5. Production Deployments
  - 앱의 라이브 서비스로 가는 마지막 단계

위의 1~5 의 단계를 이해하기에 좋은 그림이 바로 위의 그림입니다.

### GitLab

이번 그림은 제가 애용하는 GitLab 에서 가져와 보도록 하겠습니다. [GitLab Continuous Integration (GitLab CI/CD)](https://docs.gitlab.com/ee/ci/) 이라는 자료 입니다.

![GitLab 의 CI/CD Pipeline](../imgs/what-is-cicd/cicd_pipeline_infograph.png)

앞에서 비교해 보던 것들과 이제는 크게 다르지 않음을 알 수가 있습니다.

### Redhat

이번엔 레드햇! [CI/CD with OpenShift](https://blog.openshift.com/cicd-with-openshift/) 라는 자료 입니다.

![Redhat 의 CI/CD with OpenShift](../imgs/what-is-cicd/redhat-cicd.png)

### databricks

그림 정말 많이 나오죠? 아직 안끝났습니다. databricks 의 [Continuous Integration & Continuous Delivery with Databricks](https://databricks.com/blog/2017/10/30/continuous-integration-continuous-delivery-databricks.html) 입니다.

![databricks 의 CI/CD](../imgs/what-is-cicd/CI-CD-BLOG1.png)

더 볼 수도 있겠으나... 이정도면 진짜 많이 봤다고 생각합니다. 여태까지 CI/CD 의 Pipeline 을 조금씩 살펴보면서 각 단계는 어떻게 나누는지, 단계별 무슨 작업을 하는지, 정형화된 구성이 있는 지 등을 충분히 유추해볼 수 있는 시간이었겠죠.

어느 정도 개념에 대한 감은 잡혔으리라 봅니다.

## 그리고 Pipeline

위에서 쭈욱 비교해본 그림속에는 잘 보면 Pipeline 이라는 용어가 Deployment 라는 단어와 함께 계속해서 등장합니다. 느낌상 배포를 담당하는 녀석(?)같아 보입니다. 다시 위키로 돌아와 내용을 찾아 봅시다.

**Continuous delivery is enabled through the deployment pipeline. The purpose of the deployment pipeline has three components: visibility, feedback, and continually deploy.**

- **Visibility – All aspects of the delivery system including building, deploying, testing, and releasing are visible to every member of the team to promote collaboration.**
- **Feedback – Team members learn of problems as soon as possible when they occur so that they are able to fix them as quickly as possible.**
- **Continually deploy – Through a fully automated process, you can deploy and release any version of the software to any environment.**

Continuous delivery 는 deployment pipeline 을 통해 활성화되어 진다. deployment pipeline 의 목적은 다음의 3가지 컴포넌트를 가지고 있다:

- 가시성 : 협업을 장려하기 위해 팀내의 모든 멤버들에게 building, deploying, testing, releasing 을 포함한 모든 delivery system 의 측면을 볼 수 있음
- 피드백 : 누군가가 문제를 일으켰을 때 가능한한 팀 멤버들은 해당 문제에 대해 학습을 해야하고, 그걸 통해 그 문제를 빠르게 수정할 수 있음
- 지속적인 배포 : 완전 자동화된 프로세스를 통해, 어떤 버전의 소프트웨어라도 어떤 환경으로 배포가 가능

즉, Deployment pipeline 은 CI/CD 의 과정을 쭈욱 수행하기 위한 하나의 관련 절차들의 모음으로 보입니다. 배포를 담당하는 녀석이 아닌 일종의 내가 개발한, 혹은 서비스할 환경에 맞게끔 CI/CD 명세를 작성하고, 해당 명세에 따라 코드의 지속적인 통합(CI), 그리고 테스팅 또는 패키징 등의 작업을 통해 실제 서비스 환경에 올릴 수 있는 배포환경(CD)을 모두 합쳐 Deployment pipeline 이라고 불러도 될 것 같습니다. 명확한 한 단어로 설명하기가 어렵네요. 단순하게 이야기 하면 `일련의 과정들` 정도 되려나요?

다만, 국내에서 몇몇의 밋업이나 세미나, 컨퍼런스에 다녀보면 CD 라는 단어가 Delivery 인지 Deployment 인지는 확실하게 구분지어 사용하지는 않은 듯 보입니다. 어쨌든 CI/CD 는 각각의 주어진 역할이 명확히 있어 보이나, 굳이 Pipeline 을 구성할 경우 이건 CI 고 이건 CD 다 라고 경계를 만들어 주는 건 하기 나름일 듯 하네요.

## 정리하며

정리하려고 이 화면을 5분정도 쳐다 봤습니다. 다만 깔끔하게 정리되지는 않네요. 스터디 자료를 뒤늦게 정리하느라 어제 새벽에 작성한 글을 보니 맘에 안들어서 사족을 풀어 봅니다. 그래도 정보 전달을 위해 정리를 해보야 겠죠.

CI 는 굳이 설명한다면, 각자 개발한 코드를 Build 하고 Testing 해서 문제가 없다면, 해당 코드를 baseline 혹은 branch 전략에 맞게끔 병합하는 작업을 지속적으로 수행하는 것을 일컫는 것 같습니다.

그리고 CD 는 이렇게 통합되어진 코드들을 역시나 branch 전략에 맞게끔 다시 Alpha/Beta/Stage/Production 에 전달하여 환경에 맞게끔 기능적 테스트, 그 과정에서 필요한 어플리케이션 설정, 스트레스 테스트 등을 통해 rc 버전을 만들고, 이렇게 동작에 문제가 없는 것이 확인이 되었다면 최종 서비스에 배포하는 과정까지를 모두 일컫는 것 같습니다.

사실 제가 개발자가 아니라 각 배포 단계별 어떤 걸 확인해야 할지는 환경적인 요소 외에는 큰 차이를 모르겠습니다. 혹시나 제 설명이나 이해에 문제가 되는 것이 보인다면, 바로 잡아 주시면 감사하겠습니다. 언제든지 댓글로 알려 주세요. :)

여기까지 와서 CI/CD 에 대한 개념의 이해와 감을 어느 정도 잡았다면, 다음은 실제 내 프로젝트에 어떻게 사용할 수 있을지, 내 코드로는 CI/CD 를 어떻게 적용시킬 수 있을 것이며, CI/CD 의 종류(?)에 대해 알아보도록 하겠습니다. 아마도 이게 Part2 가 될 듯 하고요. :)
