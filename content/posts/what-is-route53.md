---
title: "What Is Route53"
date: 2019-05-07T20:33:05+09:00
tags: ["devops", "aws", "dns", "route53"]
categories: ["cloud"]
draft: false
---



# Route 53 의 활용

## 소개

- Route53 은 고가용성과 확장성을 지닌 도메인 네임 서비스, DNS
	- 도메인 관리
		- 새로운 도메인 등록 및 기존 도메인을 전달
	- DNS 관리 (updated.)
		- DNS 쿼리에 대한 응답으로 글로벌 네트워크 상에서 공신력(?) 있는 DNS 서버를 사용.. ~~이게 뭔 소리지..~~
			- 공신력 있는 = Authoritative
			- 공신력 없는 = Non-Authoritative
		- CloudFront 배포, Elastic Beanstalk 엔드포인트, ELB 레코드, S3 버킷에 대한 인터넷 트래픽을 라우팅, 이런 리소스들을 대상으로 한 DNS 쿼리에 대해 추가 과금도 없음
	- 트래픽 관리
	- 도메인의 Health Check
		- 웹 서버, 이메일 서버 같은 리소스에 대한 정상 동작 여부를 모니터링
		- Route53 의 상태 확인 모듈은 글로벌하게 분포되어 있음
		- 애플리케이션에 대해 접근 가능한지, 정상적으로 동작 중인지 등을 확인하기 위해 인터넷상에 요청 신호를 자동으로 보냄.. ~~이것도 이해가 안되네..~~
		- CloudWatch 알람의 환경 설정을 조정해 리소스가 사용 불가 상태일 경우 알림을 받을 수 있음
		- Fail-over 정책의 경우 사용 불가능한 상태 혹은 정상 동작 상태가 아닌 리소스로 인터넷 트래픽이 가지 않도록 재설정 작업을 지원



## Route 53 DNS 의 동작 원리

- 요청 (request) 을 공신력 있는(=Authoritative) 루트 네임 서버로 전달
	- 위의 내용 빼고는 모두 쓸데 없는 내용이라 패스
	- ~~게다가 이해도 안됨.. 이게 뭔 소리야.. 그냥 Root Hints 에 전달한다는 소린가.~~



## Route 53 에서 지원하는 DNS 리소스 레코드 타입

- A / AAAA
- CNAME
- MX
	- 메일 호스트의 도메인 네임
- NS
- PTR
- SOA
	- Start of Authority, zone 에 저장된 정보
- SPF[^1]
	- Sender Policy Framework, 정상적인 곳으로 부터의 메일인지를 확인하는 용도
	- 보통은 SPF 대신 TXT 레코드를 사용
- SRV
	- Service 레코드
- TXT



## Alias 리소스 레코드

- Route 53 은 리소스 레코드 세트와 (abc.com) TLD 에 대해 사용자 엔드포인트를 가리키도록 유연성을 제공하는 Alias 레코드를 지원한다.
	- ~~이게 무슨 소리야............~~
- CloudFront 배포, Elastic Beanstalk 엔드포인트, ELB 엔드포인트, S3 버킷, 기타 Route 53 리소스 레코드 세트를 위한 Alias 레코드 생성이 가능
- 도메인 그 자체에 대한 Alias 가 가능!! (Apex Zone 혹은 root, naked domain 이라고 부름)
	- 예를 들어 google.com 에 대한 Alias 가 가능
		- 확인 필요, 모든 경우에 사용가능하진 않고 제한적인 지원으로 보임
	- bind 는 지원하지 않음[^2]
		- CNAME 이 정의되면, 다른 RDATA 는 정의되어선 안됨
		- 하나의 RR 에는 CNAME 이 있는 경우 다른 RR 을 선언할 수 없음(MUST)[^3]
			- 또한 Apex of zone 은 A Record 만 허용되도록 정의
		- DNSSEC 의 경우 예외[^4]



## 라우팅 정책

- 단순 라우팅, Simple Routing Policy
	- 트래픽을 라우팅 하는 도메인을 위한 리소스가 하나만 있을 경우
	- ex. A record
- 가중치 기반 라우팅, Weighted Routing Policy
	- 동일한 역할을 수행하는 리소스가 여러 대 있을 경우
	- ex. 60%/40% 의 비율로 트래픽을 라우팅
	- 50/50 일 경우 Active-Active 와 같이 동작
- 지연 시간 기반 라우팅, Latency-based Routing Policy
	- 네트워크 레이턴시가 제일 낮은 사용자를 우선으로 라우팅
- 장애 조치 라우팅, Failover Routing Policy
	- Active-Standby 형태의 구성
	- 평상시 한대에서만 모두 처리, 나머지는 Idle 상태
- 지리적 라우팅, Geolocation Routing Policy
	- GeoIP DB 를 사용, LDNS 의 지리적 위치를 기반으로 쿼리에 응답



## 신규 도메인 등록

- 책에서는 실제 도메인을 등록하는 과정, 돈 들어가니 일단 패스
- 직접 구매 후 등록할 경우 호스팅 영역이 자동으로 등록
	- 도메인 등록 후 Route 53 이 리턴하는 값들
		- Registered on : 도메인을 등록한 날짜
		- Expires on : 도메인이 만료되는 날짜 및 시간
		- Domain name status code : 도메인의 현재 상태
			- Registering, transferring, renewing  등
		- Transfer Lock : 활성(enabled) 또는 비활성(Disabled)
		- Auto renew : Route 53 이 자동으로 도메인을 갱신할지 여부를 나타냄
		- Authorization code : 구매 후 등록업체를 다른 곳을 사용할 경우에 필요
		- Name servers : 네임 서버 목록



## 도메인을 Amazon Route 53 으로 전송하는 방법

- 현재 사용 중인 등록 업체에서 Route 53 으로 도메인을 전달할 수 있음
	- 전달? = transfer? 를 말하는 건가. ~~아니면 위임?(delegation)~~
	- 내용상 다른 곳에서 운영 중엔 도메인에 대한 Transfer 방법
- 다음과 같은 최소 요건이 존재
	- 최소 60일 정도 유효한 도메인
	- 현재 도메인 상태가 다음 중 하나에 해당되지 않을 것
		- pendingDelete
		- pendingTransfer
		- redemptionPeriod
		- clientTransferProhibited
	- Route 53 이 지원하는 TLD 일 것
		- 참고로 내가 사용하는 도메인의 경우 지원하지 않는 TLD... ~~망할.~~
		- 스터디를 위해 급 TLD 를 하나 구매 했더니만, Pending requests 에 걸리며 구매가 바로 되지 않음.. 허허
			- 급 ep1. 하도 오래전에 free tier 를 등록해 뒀더니 등록된 카드가 만료되서 구매에 실패함.
			- 급 ep2. payment method 를 변경하고 바로 다시 같은 도메인을 또 구매했더니 그 전에 실패한 메일이 와서 카드에 문제가 있었는 줄...
			- 급 ep3. 어쨌거나 저쨌거나 이번에 구매한 도메인은 스터디에 사용하긴 망한 듯.....할줄 알았으나,
			- 급 ep4. 좀 유별난 TLD 임에도 불구하고 약 15분 만에 등록 완료!
		- 생각보다 Route 53 에서 지원하는 TLD 가 꽤 많음[^5]
- 책 내의 전송(=Transfer)하는 방법에 대한 수행 방법은 단순 참고만 할 것
	- 실제 도메인 관리 업체를 바꾸지 않는 한 도메인을 통째로 들고 옮기는 케이스는 많지 않음



## 호스팅 영역과 레코드 세트 생성

- 호스팅 영역
	- 트래픽을 어떻게 라우팅할지에 대한 정보를 RRSet 형태로 저장
	- 레코드는 DNS 가 도메인, 서브도메인에 어떻게 응답할지를 결정하는 역할
	- 도메인은 등록이 완료되지 않았더라도 구매한 도메인에 대한 호스팅 영역은 이미 만들어져 있음을 확인
		- 도메인은 등록이 안되어 있는데 이걸 어떻게 사용할지는 잘 모르겠음.
- 호스팅 영역 생성하기
	- Type 에 2가지가 존재
		- Public Hosted Zone
			- 실제 구매한, Public 도메인의 경우에 해당
		- Private Hosted Zone for Amazon VPC
			- 확인 필요, 아마도 VPC 내부용, 즉 아마존 외부에서 조회되지 않는 임의의 도메인으로 추정
	- 호스팅 영역이 생성되면,
		- 기본적으로 NS 와 SOA 레코드가 만들어짐
		- 글로벌로 전파되기 위한 약간의 시간을 가진 후 바로 `dig` 로 조회 가능



## 퍼블릭 호스팅 영역 삭제

- 호스팅 영역 삭제 전 레코드 삭제가 먼저 필요
- 그 다음 호스팅 영역 삭제가 가능



## Alias 레코드 세트 생성

- ELB, CloudFront, S3 는 하나 이상의 IP 주소를 이용해 각 요청에 대해 응답하고, IP 주소가 자주 변경
- 이러한 서비스들을 IP 주소를 기반으로 운영하면 안됨
- 이에 대한 대안으로 CNAME 레코드를 추가
	- Alias : Yes 를 선택 후 연결할 자원을 선택
- Alias 를 이용할 경우 다음의 RRSet 에 대해서는 TTL 을 설정할 수 없음
	- CloudFront, Elastic Beanstalk, ELB, S3 버킷
- Alias 를 이용할 경우 등록된 자원의 변경 사항을 Route 53 이 자동으로 인지, 별도로 변경이 필요 없음
- ELB 혹은 CloudFront 의 IP 주소가 달라질 경우 자동으로 DNS 쿼리에서 변경 사항을 반영
- Alias 는 Route53 내부에서만 유효
	- Alias 의 RRSet 과 대상 모두 AWS 의 리소스 여야만 함
- CloudFront 배포, Elastic Beanstalk, ELB, S3 에 대한 Alias 사용 시 별도의 요금이 없음



## 프라이빗 호스팅 영역 생성

- VPC 내의 도메인과 서드도메인에 대한 트래픽을 어떻게 라우팅할 지에 대한 정보를 담고 있음
- 생성 시 단일 VPC 만 지정 가능
	- 나중에 VPC 를 호스팅 영역으로 추가할 수 있음
		- ~~역시 이해 안됨..~~
		- 프라이빗 호스팅 영역 생성 시 다음의 VPC 설정이 True 여야 함
			- `enableDnsHostnames`
			- `enableDnsSupport` 
				- 이건 어디서 설정을?
- 음.. 프라이빗 호스팅 영역 생성 후 자동으로 ec2 의 private dns 가 변경되는 것인줄 알았으나 그건 또 아님.
	- 별도로 다시 RRSet 을 지정해줘야 동작하는 것 같음
	- 단순 테스트결과 외부 질의되지 않는 존 파일 형태로 보임
		- 내부 통신용으로 활용 가능할 듯



## 가중치 기반 라우팅 정책 활용 방법

- 동일한 기능을 수행하는 여러 대의 리소스가 있을 경우 적용
- 말 그대로 가중치 기반
- 가장 많은 활용처는
	- 로드 밸런싱
	- A/B 테스팅
- 단순 A Record 등록 후 Routing Policy 항목에서 Weighted 를 선택
	- `Set ID` 를 통해 동일한 RRSet 내에서 구분할 수 있는 고유 ID 가 됨
	- 같은 RR 이름을 가지되 등록하는 A Record 의 Value(=IP)를 Weighted 와 함께 등록하면 댐
- 전체 리소스의 가중치 총합과 리소스 각각이 지닌 가중치와의 비율을 바탕으로 리소스가 선택되는 방식
	- 즉 굳이 100% 를 맞출 필요 없음. 



## 장애 조치 라우팅 정책과 상태 확인 활용 방법

- Failover Routing Policy 를 의미
- 정상적으로 동작하고 있지 않은 리소스의 트래픽을 정상적으로 동작 중인 리소스로 라우팅하는 방법
	- Health Check
		- 설정 자체는 매우 단순
		- 프라이빗 호스팅 영역에 있는 리소스의 경우 H/C 생성이 불가능
		- H/C 는 VPC 밖에서만 사용 가능
		- 공인 IP / DNS 리소스에 대한 H/C 생성이 가능



## dig

```bash
[19/05/7 9:44:22] ❯ dig mail.google.com @168.126.63.1

; <<>> DiG 9.10.6 <<>> mail.google.com @168.126.63.1    ## dig version (9.10.6) 과 command line parameter
;; global options: +cmd    ## query option
;; Got answer:  ## 응답받은 dns query header
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 62191  ## opscode: QUERY, status: NOERROR
;; flags: qr rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 4, ADDITIONAL: 5  ## 2개의 ANSWER, 4개의 AUTHORITY, 5개의 ADDITIONAL 이 있다는 정보
## DNS Flag
### : qr (query/response)
### : rd (recursion desired)
### : ra (recursion available)

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:    ## 어떤 record 로 어떤 type 의 query 를 요청했는지 확인, IN 은 Internet Lookup 을 의미
;mail.google.com.		IN	A

;; ANSWER SECTION:    ## mail.google.com 의 응답은 CNAME 임을 알려줌
mail.google.com.	40859	IN	CNAME	googlemail.l.google.com.
googlemail.l.google.com. 283	IN	A	216.58.197.229    ## 다시 해당 CNAME 에 대한 A Record 를 확인

;; AUTHORITY SECTION:  ## google.com 이라는 도메인을 관리하는 서버의 정보를 표시, 4대
google.com.		127253	IN	NS	ns1.google.com.
google.com.		127253	IN	NS	ns3.google.com.
google.com.		127253	IN	NS	ns2.google.com.
google.com.		127253	IN	NS	ns4.google.com.

;; ADDITIONAL SECTION:  ## 도메인 관리 서버의 추가 정보 표시, 불필요한 추가 질의를 방지
ns1.google.com.		135572	IN	A	216.239.32.10
ns2.google.com.		130695	IN	A	216.239.34.10
ns3.google.com.		134156	IN	A	216.239.36.10
ns4.google.com.		131004	IN	A	216.239.38.10

;; Query time: 7 msec  ## stats 과 관련된 정보들
;; SERVER: 168.126.63.1#53(168.126.63.1)
;; WHEN: Tue May 07 21:44:22 KST 2019
;; MSG SIZE  rcvd: 223

```

- Authoritative Nameserver
	- 특정 도메인에 대한 관리 권한이 있는 DNS 서버, NameServer
	- 즉, 다른 곳에 가서 질의하지 않고 내가 가진 데이터로 직접 응답을 줄 수 있는 NameServer
- Recursive Resolver
	- Root hints 혹은 Forwarders 로 query 를 할 수 있는 DNS 서버
	- Non-authoritative query 를 다른 DNS 로 전달 가능한 DNS 서버
	- 즉, 나에게 정보가 없지만 다른 곳에 가서 질의하여 응답을 줄 수 있는 NameServer



[^1]: KISA, SPF 설치 및 운영 지침서 - 불법스팸대응센터, https://spam.kisa.or.kr/filemanager/download.do?path=spam/customer/archive/&filename=SPF%EC%84%A4%EC%B9%98%EB%B0%8F%EC%9A%B4%EC%98%81%EC%A7%80%EC%B9%A8%EC%84%9C.pdf
[^2]: ISC, CNAME at the apex of a zone, https://kb.isc.org/docs/aa-01640
[^3]: IETF, RFC1033, Domain Administrators Operations Guide, https://www.ietf.org/rfc/rfc1033.txt
[^4]: IETF, CNAME at the zone apex , https://tools.ietf.org/id/draft-sury-dnsext-cname-at-apex-00.html
[^5]: AWS, Amazon Route 53에 등록할 수 있는 도메인, https://docs.aws.amazon.com/ko_kr/Route53/latest/DeveloperGuide/registrar-tld-list.html