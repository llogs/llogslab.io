---
title: "AWS VPC #02"
date: 2019-04-22T21:28:38+09:00
tags: ["aws", "network", "vpc"]
categories: ["cloud"]
draft: false
---

# VPC 고급 구성 요소

## 한 개의 인스턴스， 한 개의 ENI 에 여러 개의 IP를 할당하는 방법

- ENI (Elastic Network Interface) 는 가상의 인터페이스
- 각각의 인스턴스에 ENI 를 attach/detach 할 수 있으며, attach 할 수 있는 ENI 의 갯수는 EC2 인스턴스 타입마다 다름
  - ENI 는,
    - 하나의 primary IP 주소와 여러개의 secondary IP 주소를 가짐, 경우에 따라 Public IP 이거나 EIP 일 수 있음
    - 인스턴스 타입과 네트워크가 지원될 경우 하나 이상의 IPv6 주소를 가짐
    - MAC 주소는 오로지 1개만 가짐
    - SG, src/dest check 와 같은 추가 속성도 가지고 있음
- EC2 에 attach 된 ENI 의 VPC 와 Subnet 은 EC2 가 속해 있는 VPC 와 Subnet 과 동일해야 함



## 네트워크 안에 있는 인스턴스에 엑세스 하는 방법

- 각각 Public / Private Subnet 에 인스턴스를 생성
- Private Subnet 의 인스턴스의 경우, AWS Console 에서 VPC 의 IGW 로 인해 공인 IP 로 할당된 AWS Domain 은 보이나 실제 접속은 불가능 (= R/T 가 없으므로)
- 이러한 경우를 위해 Bastion or Jump 서버를 구성
  - 단순히 Public Subnet 의 인스턴스를 통해 Private Subnet 인스턴스로 접속하는 형태
  - 같은 로컬 네트워크(같은 VPC 내의 CIDR 블록이라면) S/G 정도의 보안 설정만으로도 접근은 가능



## 애플리케이션 ELB 생성

- ELB (Elastic Load Balancer) 는 트래픽 로드 밸런싱을 담당, 아래와 같은 3개의 형태[^1]
  - Classic ELB
    - EC2 인스턴스들의 단일 셋에 대한 요청을 포워드, L4
    - EC2 인스턴스가 Class 네트워크 내에 있는 경우 필수
  - ALB (Aplication Load Balancer)
    - 애플리케이션의 콘텐트, 경로, 호스트 등을 기준으로 한 애플리케이션의 요청을 포워드, L7 
  - NLB (Network Load Balancer)
    - 고성능의 LB
- ALB 생성 시 하나 이상의 AZ 에 서로 다른 Subnet 이 필요



## Launch configuration 생성

- Launch configuration 은 사전에 정의한 환경 설정 정보를 그대로 이용해 EC2 인스턴스를 생성하기 위한 템플릿을 만드는 방법
- 로드밸런서에 인스턴스를 쉽게 추가할 수 있음
- Auto Scaling 그룹을 만들기 전 Launch configuration 을 만들어 두는 것이 좋음
  - 이 중 `Configuration` > `Advanced Details` > `Do not assign a public IP address to any instances` 를 선택하여 로드밸런서를 통해서만 인스턴스에 엑세스 할 수 있게 할 것
- 이 문서를 작성하는 현재 `Launch Template` 이 나온 듯



## 오토 스케일링 그룹 생성

- 동일한 애플리케이션을 호스팅하는 인스턴스의 그룹
- 이 그룹의 인스턴스들은 인스턴스를 관리하고 확장시키기 위한 논리적 그룹으로 취급
- 애플리케이션상의 부하에 따른 성능 저하가 일어나지 않도록 인스턴스가 몇 대 정도면 적절한 지 필요한 수량을 정의할 수 있음
- 오토 스케일링 그룹의 크기를 늘리고 줄일 수 있도록 `Add notification` 을 등록
  - 다양한 파라미터와 조건의 생성이 가능



## VPC 피어링 생성

- 임의의 VPC 에 속해 있는 인스턴스들은 다른 VPC 의 인스턴스와 격리되어 있음
- Private IPv4/6 주소를 이용해 인스턴스들 간에 통신을 하기 위해서는 VPC 피어링을 생성해줘야 함
- 동일한 Region 내에 있는 2개의 VPC 들 사이에 피어링 생성이 가능
- 이들 VPC 는 동일한 AWS 계정 혹은 서로 다른 계정에서 생성되었을 수 있음
  - 내 계정이 아닌 VPC 의 경우 해당 계정의 `Account ID` 를 알고 있어야 함
  - 또한 수신측에서 피어링 요청을 수락해줘야 함
  - VPC 의 라우팅 테이블에 네트워크 트래픽의 활성화 역시 필요
- 수신측에서 수락 후 DNS resolution 을 허용하도록 설정
- 모든 구성이 완료된 후 R/T 의 수정 역시 필요, Peering Connection 타입의 R/T 를 추가
- 제약 사항
  - CIDR 대역이 겹치는 VPC 들 간에는 피어링 생성이 불가능
  - 서로 다른 Region 에 있는 VPC 들 간에는 피어링 생성이 불가능
  - VPC 피어링은 uRPF 를 지원하지 않음
  - VPC 는 transit 역할을 하지 않음 (= 트래픽 중계를 하지 않는 다는 의미)



## Amazon VPC 에 대한 VPN 연결 관련 환경 설정 방법

- 기본적으로 데이터센터 내에 있는 인스턴스와 VPC 내에 있는 인스턴스들 간에는 안전한 통신이 이루어질 수 없음
- 이를 해결하기 위해 IPSec VPN 설정이 필요
- 다음의 세가지 요소로 구성
  - VPG (Virtual Private Gateway), AWS 쪽의 VPN 연결을 담당, PVC 내에서 2개의 IPSec 터널링을 제공
  - CGW (Customer Gateway), 고객 데이터센터 쪽의 VPN 연결을 담당, 고가용성을 위해 여러개의 CGW 를 설정하는 것이 좋음
  - VGW 와 CGW 를 연결하는 VPN Connection 이 필요
- VPG 는 VPC 에 attach 하는 구조
- CGW 는 VPG 에 연결
- VPN Connection 을 통해 VPG 와 CGW 를 연결
- 최종 구성 후 라우터 등의 환경설정을 다운로드 하여 데이터센터 내의 라우터를 원하는 대로 설정
  - 벤더, 플랫폼, S/W 등을 선택하여 다운로드



# Q/A

- Q : 하나의 EC2 인스턴스에 서로 다른 Subnet 의 ENI 를 attach 할 경우 바라보는 D/G 는 어디일까? 그리고 그 때의 R/T 은 어떠한 형태일까?
  - A : 두 개의 ENI 모두 D/G 를 바라보고 있음. 그러나 인터넷 연결이 불안정한(?) 형태는 아님. DNS 확인 결과 eth0 인 기본 ENI 로 설정되어 있음.
- Q : ALB 를 생성할 때 반드시 하나 이상의 AZ 에 속한 Subnet 을  두개 이상 설정해줘야 하는 이유는 뭘까? 이말은 외부에서의 트래픽을 처리할 ALB 라면, 두개의 Subnet 모두 IGW 와 연결되어 있어야 한다는 의미.
  - A : 





[^1]: AWS, Elastic Load Balancing 제품 비교, <https://aws.amazon.com/ko/elasticloadbalancing/features/#compare>

