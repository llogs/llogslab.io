---
title: "AWS VPC #01"
date: 2019-04-10T21:28:38+09:00
tags: ["aws", "network", "vpc"]
categories: ["cloud"]
draft: false
---

# AWS 네트워킹 구성 요소

## AWS 네트워크 서비스

- Region
  - AWS 는 복수 개의 AZ 로 구성된 리전을 20 여개 운영 중[^1]
  - 각 Region 은 다른 Region 과 완전히 격리되어 있음
  - 네트워크 구성 요소를 생성하기 위해서는 1개의 Region 을 선택해야 함
- AZ (Availability Zone)
  - 각 Region 의 물리적으로 분리된 복수개의 데이터 센터로 구성
  - 일부 네트워크 구성 요소는 VPC 처럼 Region 내의 여러 AZ 에 걸쳐 확장이 가능
    - Subnet 의 경우 AZ 내부로 제한
      - AWS VPC
        - 데이터센터 내의 네트워크 처럼 AWS 클라우드에서 구축되는 가상 네트워크 환경
        - 하나의 VPC 는 다른 VPC 와 완전히 격리되어 있음
        - VPC 의 CIDR 범위를 필요한 만큼 선택한 후, 이 범위 내에서 IP 주소를 할당한 EC2 인스턴스를 생성
        - 기존 네트워크 관점에서의 VRF 로 생각하면 됨
        - VLAN 은 당연히 Subnet, 분리된 VLAN 또는 Subnet 내에서 EC2 인스턴스의 격리가 가능



### VPC 생성 하기

- VPC 를 생성하는 방법은
  - VPC Dashboard > Launch VPC Wizard 를 사용하는 방법 1
    - VPC 생성과 동시에 Subnet 을 생성
  - VPC Dashboard > Your VPCs > Create VPC 를 사용하는 방법 2
    - VPC 생성 후 Subnet 을 별도로 생성

모두 2가지 방법이 제공, 그러나 **VPC with a Single Public Subnet** 을 만드는 데에는 큰 차이가 없으니 아무거나 사용해도 무방



1. Launch VPC Wizard 를 사용

![image-20190415201439098](../imgs/image-20190415201439098.png)

![image-20190415201548518](../imgs/image-20190415201548518.png)

2. Create VPC 를 사용

![image-20190415201910153](../imgs/image-20190415201910153.png)



#### 생성된 VPC 에 IPv6 EC2 연결하기

- 굳이 IPv6 를 연결할 필요는 없으므로, 단순히 EC2 인스턴스 하나를 생성하여 바로 연결
- VPC Wizard 를 사용했을 경우엔 Internet Gateway(이하 IG) 까지 알아서 생성되지만, 단순 Create VPC 를 사용했을 경우엔 IG 를 직접 생성하여 연결해 주어야 외부에서 조회가 가능
- IG 의 경우 VPC 별 연결이므로, 한번 IG 가 연결된 VPC 에 신규 EC2 인스턴스를 연결하면 바로 외부 통신이 가능
- VPC 에 EC2 를 연결하는 과정은 다음과 같음
  - EC2 > Launch Instance > Step 3: Configure Instance Details 에서 이미 생성된 Network(=VPC) 와 Subnet (=in VPC) 를 선택해주면 됨



#### EC2  인스턴스상에서 NAT 생성

- 퍼블릭 서브넷의 EC2 인스턴스는 IG 를 통해 외부와 직접 통신이 가능
  - 퍼블릭 서브넷 : 서브넷 트래픽이 IG 로 라우팅 되는 경우, 해당 서브넷을 퍼블릭 서브넷이라고 부름[^2]
  - 프라이빗 서브넷 : 연결할(=attach) IG 가 없으므로, 해당 서브넷의 EC2 인스턴스는 외부와 직접 통신할 수 없음
- NAT 인스턴스에 연결할 Elastic IP(이하 EIP) 생성
  - EC2 > Elastic IPs > Allocate new address
  - EIP 의 Scope 는 VPC
- 생성한 EIP 를 NAT 인스턴스에 연결
  - 생성한 EIP 선택 > Associate address > Instance
    - 이미 할당된 EIP 가 있을 경우 진행되지 않음
      - 강제로 relocate 옵션을 주고 진행
    - 아마도, IG 에 연결된 VPC 는 자동 할당인 것으로 보임
  - EIP 에 연결된 경우 EC2 > Instances 에서 보면 IPv4 Public IP 에 링크가 걸림
- Source/dest. check 옵션
  - 각각의 EC2 인스턴스는 기본적으로 Source/dest. check 를 수행
  - 이는 인스턴스가 보내거나 받는 트래픽의 Source 또는 Dest. 여야 한다는 의미
  - 그러나 NAT 인스턴스의 경우 Source 또는 Dest. 가 자기 자신이 아닐 때 트래픽을 보내고 받을 수 있어야 함
    - 네트워크 기본적으로 목적지 주소가 내가 아닌 트래픽이 들어오면 Drop.
  - 따라서, Source/dest. check 옵션의 비활성화가 필요[^3]



### VPC 구성 요소들의 종속관계

- VPC 는 Region 에 종속적
  - Region 내 모든 AZ 에 적용
- Subnet 은 하나의 AZ 에 종속적
  - 여러 AZ 로의 확장은 불가능



### 예약된 VPC CIDR 주소들

- 10.172.0.0/16 이 내 VPC CIDR 대역이라면,[^4]
  - 10.172.0.0 : 네트워크 주소
  - 10.172.0.1 : AWS VPC 라우터 주소 (아마도 Subnet 들의 G/W, 로컬 라우터 주소)
  - 10.172.0.2 : CIDR 내 VPC DNS 서버 주소 (해당 CIDR 내 모든 VPC 가 동일)
    - 또한 각 Subent 기준 +2 의 주소 역시 예약
    - CIDR 이 여러개인 VPC 의 경우 DNS 서버의 주소는 기본 CIDR 에 위치[^5]
  - 10.172.0.3 : Reserved
  - 10.172.0.255 : Broadcast 주소, VPC 에서는 Brodcast 를 지원하지 않으므로 미리 예약
- Subnet 의 경우는 확인이 필요
  - 아마도 비슷할 것으로 예상



### 기타

#### Amazon VPC 제한[^6]

- VPC 에 허용된 CIDR 의 크기는 /16 ~ /28
- VPC 는 Region 당 5개로 제한, 별도 요청을 통해 증설이 가능
- VPC 당 서브넷 은 200개



[^1]: AWS, AWS 글로벌 인프라, https://aws.amazon.com/ko/about-aws/global-infrastructure
[^2]: AWS, VPC 및 서브넷, https://docs.aws.amazon.com/ko_kr/vpc/latest/userguide/VPC_Subnets.html#vpc-subnet-basics
[^3]: AWS, NAT 인스턴스, https://docs.aws.amazon.com/ko_kr/vpc/latest/userguide/VPC_NAT_Instance.html
[^4]: AWS, VPC 와 Subnet, https://docs.aws.amazon.com/ko_kr/vpc/latest/userguide/VPC_Subnets.html
[^5]: AWS, Amazon DNS 서버, https://docs.aws.amazon.com/ko_kr/vpc/latest/userguide/VPC_DHCP_Options.html#AmazonDNS
[^6]: AWS, Amazon VPC 제한, https://docs.aws.amazon.com/ko_kr/vpc/latest/userguide/amazon-vpc-limits.html







