---
title: Ubuntu 16.04 에 Docker 설치하기
subtitle: Docker CE 설치 w/proxy
date: 2018-04-16
tags: ["docker", "ubuntu", "proxy"]
---

제목 부터가 식상한 포스팅입니다.

docker 가 세상에 나온 지 어림잡아 5년이 지난 마당에 누가 이런 포스팅을 정리하겠냐고 생각할 수 있겠지만, 저같은 기억력 빼꼼이 들에겐 기록보다 좋은 건 없습니다. 단 한번도 docker 를 제대로 살펴본 적이 없어, 이 기회에 한번 글로 같이 정리해 두고자 합니다.

게다가 보통의 강력한 보안 정책을 가진 회사 내에선 공인IP 를 가진 서버로 테스트 하기가 쉽지 않습니다. 설치하기를 따라하면서 필요한 Proxy 설정도 함께 진행해 보고자 합니다.

이렇게 아무것도 모르는 무지랭이 상태에서 무언가에 접근하기 제일 편한 방법은 공식 문서를 읽어 보는 것일 겁니다. 사실 예전에도 이런저런 뻘짓을 하며 해당 문서를 읽어 보긴 했지만, 제대로 읽어본 기억은 없네요. 게다가 공식 문서에는 조금 설치가 까다롭기도 하고 이참에 처음 보는 척 하면서 다시 보도록 하겠습니다.^[Install Docker, https://docs.docker.com/install/]

docker  에는 2가지 에디션이 있으며, 이중 Enterprise Edition (EE) 의 경우 어차피 내가 설치하지 못할 것 같아 보이니, 거들떠도 보지 않고 과감하게 pass, CE, Community Edition 으로 설치를 해보도록 합니다. 대충 봐도 공부용으론 CE 로도 충분해 보입니다.^[Get Docker CE for Ubuntu, https://docs.docker.com/install/linux/docker-ce/ubuntu/]

자, 얼마나 친절한 공식 가이드 인지 어디 한번 따라가 봅시다.

# 기존의 Docker 제거하기

아래의 명령어를 통해 기존에 구성된 docker 가 있다면 삭제해야 할 것 같습니다. 일단 그대로 삭제를 진행.

`$ sudo apt-get remove docker docker-engine docker.io`

당연히 없으므로 별 문제 없이 통과되었네요. 
참고로 위의 명령어를 실행해도, images, containers, volumes, networks 등을 포함한 /var/lib/docker/ 의 컨텐츠들은 그대로 남겨지는 것 같네요. 딱히 기존 설치 버전이 없어 확인은 하지 않았습니다.

그리고 Docker CE 는 이제 docker-ce 라고 불리워 지나 봅니다.

docker-ce 에서는 `overlay2` 와 `aufs` 라는 Storage Driver 들을 지원하는데, 이는 각 Linux Kernel 버전 마다 다르다고 합니다.

* Kernel 4.x 또는 그 이상의 경우
    * aufs 보단 overlay2 를 선호, 지원. (사용할 것 정도로 이해하면 될 것 같습니다.)
* Kernel 3.x 인 경우
    * aufs 만 지원, overlay 또는 overlay2 를 kernel level 에서 지원하지 않음 (그냥 aufs 쓰란 이야기)

Ubuntu 16.04 또는 그 이상인 경우, OverlayFS 를 지원하며 docker-ce 는 overlay2 storage driver 를 기본적으로 사용합니다. 뭔 차인지 물어 보진 마세요. 저도 모릅니다. aufs 를 사용해야 할 경우엔 별도의 수동 설정이 필요하다고 하지만, 전 16.04 에 설치하니 이 과정은 역시나 불필요하니 따로 다루지 않겠습니다. 굳이 궁금하면 직접 찾아 보시면 될 것 같습니다.

# docker-ce 설치하기

설치 방법 역시 여러가지가 있지만, 아무래도 가장 쉬워보이는 건 아래 방법인듯 합니다.

`$ curl -fsSL https://get.docker.com/ | sudo sh`

그러나 이미 공식 문서를 보기로 시작한 이상 계속 가이드를 따라가 보겠습니다.
(위의 내용 역시 공식 가이드엔 나와 있습니다.^[Install using the convenience script, https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-convenience-script] 다만 Production 환경에선 사용하지 않을 것을 권고하고 있습니다.)

## Repository 를 사용하여 설치

여러가지 중 docker stable repository 를 설정하여 docker-ce 를 설치하는 방법을 확인해 보겠습니다.

### stable repository 설정

우선 현재 package 상태를 최신으로 update 합니다.

`$ sudo apt-get update`

그 후, HTTPS 를 사용하여 repository 에 접근하도록 다음과 같이 설정합니다.

```
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
```

그럼 뭔가 막 설치될 겁니다. 그 후 다음과 같이 docker 의 공식 GPG key 를 추가합니다.

`$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`

저의 경우는 바로 이 순서에서 문제가 발생하기 시작했습니다. 회사에서 사용하는 가상서버의 경우 사설IP 가 기본이라 외부와의 통신이 단절되어 있기 때문인데요, 따라서 proxy server 를 다음과 같이 설정한 후 다시 시도하여 해결했습니다.

```
export http_proxy=http://proxy서버주소:포트
export https_proxy=http://proxy서버주소:포트
```

어쨌든 GPG Key 를 가져와서 추가한 후, fingerprint 를 확인해 봅니다. 전체를 확인할 필요는 없고 마지막 8자리만 확인하면 된다고 하니 그대로 확인.

```
$ apt-key fingerprint 0EBFCD88
pub   4096R/0EBFCD88 2017-02-22
      Key fingerprint = 9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
uid                  Docker Release (CE deb) <docker@docker.com>
sub   4096R/F273FCD8 2017-02-22
```

그 후 stable repository 를 설정합니다.
```
$ add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

별 에러 메시지가 발생하지 않는다면, 그대로 통과.

이 글을 쓰는 현재의 docker-ce release notes 를 확인하면 최신 버전은 18.03 으로 보입니다.^[Docker CE release notes, https://docs.docker.com/release-notes/docker-ce/] 아마도 이 버전이 설치될 것 같네요. 

만약 kubernetes 를 위해 docker 환경을 설정하는 거라면 kubernetes 1.10 기준 docker 17.03 버전이 설치되어야 합니다.^[kubernetes/CHANGELOG-1.10.md, https://github.com/kubernetes/kubernetes/blob/master/CHANGELOG-1.10.md#external-dependencies] 처음 아무것도 모르고 냅다 rancher 구성 후 kubernetes 를 설치 했을 때 자꾸만 문제가 발생해서 처음엔 docker-ee 의 문제인지 알았는데, 결국 docker 의 버전 문제 였습니다. 18.03 을 제거하고 17.03 으로 재설치 후 설치가 되었었습니다.

kubernetes 1.10 의 Changelog 중 External Dependencies 에 보면 다음과 같이 안내되어 있습니다.

`The validated docker versions are the same as for v1.9: 1.11.2 to 1.13.1 and 17.03.x (ref)`

### docker-ce 설치하기

docker 의 stable repository 를 설정했으니 apt package index 를 업데이트 합니다.

`$ sudo apt-get update`

아마 보통의 경우 별 문제 없이 진행되겠지만, 저의 경우는 역시나 사설IP 환경에 있기 때문에, apt 를 위한 별도의 proxy 설정이 또 필요합니다.
다음과 같이 설정해 줍니다.^[apt get - Ubuntu 16.04 apt-get not working through proxy - Stack Overflow, https://stackoverflow.com/questions/38591415/ubuntu-16-04-apt-get-not-working-through-proxy]

/etc/apt/apt.conf.d/[임시파일명]
```
Acquire::http::proxy  "http://proxy서버주소:포트";
Acquire::https::proxy "http://proxy서버주소:포트";
```

그 후 다시 `apt-get update` 를 실행하면 다음과 같은 화면을 ~~드디어~~ 확인할 수 있습니다.
```
Get:5 https://download.docker.com/linux/ubuntu xenial InRelease [65.8 kB]
Get:6 https://download.docker.com/linux/ubuntu xenial/stable amd64 Packages [3,539 B]
```

이제는 정말 docker-ce 를 설치할 차례입니다.

`$ sudo apt-get install docker-ce`

완료된 후 간단히 설치를 확인하고자 할 땐 다음과 같이 입력해 봅니다.

`$ docker run hello-world`

과연 그대로 될까요? 어디 다시 한번 해봅시다.
```
# docker run hello-world
Unable to find image 'hello-world:latest' locally
docker: Error response from daemon: Get https://registry-1.docker.io/v2/: net/http: request canceled while waiting for connection (Client.Timeout exceeded while awaiting headers).
See 'docker run --help'.
```

역시 안됩니다. 이놈의 사설IP 환경은 뭐 하나 제대로 되는 게 없습니다. docker 역시 proxy 사용을 위한 별도의 설정을 진행해 줘야 합니다.
이곳^[Control Docker with systemd, https://docs.docker.com/config/daemon/systemd/]을 참조하여 다음과 같이 설정하면 됩니다.

참고로, 위의 문서에는 HTTP/HTTPS Proxy 설정외에 docker 를 systemd 사용을 통해 제어하는 방법이 설명되어 있습니다. 물론 전 필요 없으니 안읽습니다. 여튼.

아래와 같이 docker 서비스를 위한 디렉토리를 하나 먼저 생성합니다.

`$ sudo mkdir -p /etc/systemd/system/docker.service.d`

그 후 해당 경로 아래 설정 파일 하나를 다음과 같이 생성합니다. 이름이 반드시 저래야 하진 않을 것 같은데, 찜찜하니 가이드를 따라 적습니다.

`$ sudo vi /etc/systemd/system/docker.service.d/http-proxy.conf`

그리고, 다음과 같이 HTTP/HTTPS 를 위한 PROXY 설정을 추가해 줍니다.

```
$ cat /etc/systemd/system/docker.service.d/http-proxy.conf
[Service]
Environment="HTTP_PROXY=http://proxy서버주소:포트"
```

변경된 내용 적용을 위해 다음과 같이 docker 를 재시작합니다.

```
$ sudo systemctl daemon-reload
$ sudo systemctl restart docker
```

제대로 설정이 되었다면, 아래의 명령어를 통해 확인이 가능 할 겁니다.
```
$ systemctl show --property=Environment docker
Environment=HTTP_PROXY=http://proxy서버주소:포트
```

자, 다시 docker 정상 설치 확인을 위해 다음과 같이 실행해 봅시다.
```
# docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
9bb5a5d4561a: Pull complete
Digest: sha256:f5233545e43561214ca4891fd1157e1c3c563316ed8e237750d59bde73361e77
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/engine/userguide/
```

드디어 정상적인 결과가 나왔네요!
일단 여기까지.

결론.<br>
사설IP 환경에서의 docker 설치 과정은 꽤나(~~거업나~~) 귀찮습니다.
