---
title: Jenkins 와 GitLab 연동하기
subtitle: Jenkins 설치 부터 GitLab push notification 연동까지
date: 2018-10-11
tags: ["jenkins", "docker", "GitLab", "git", "ci/cd"]
---

# 왜 이제와 Jenkins 와 GitLab 인가

요 근래 새로 시작한 스터디 덕분에 CI/CD 를 공부할 명분이 생겼습니다. IT 인프라 쪽이라면 한 번 이상은 들어봤을 단어이고, 익숙해진 단어지만 발표 내용을 보면 뭔진 알겠는데, 그래서? 라는 질문이 항상 있던 단어들이었죠. 이 참에 알아 보기로 했습니다.

그런데 왜 제목이 CI/CD 와 관련된 게 아니라 Jenkins 와 GitLab 연동하기 일까요? 사용하는 게 중요하지 설치가 뭐가 중요하겠어? 라고 생각했는데 의외로 GitLab 과 연동한 자료들이 오래되어서 현재와 맞지 않거나, 혹은 공식 문서 자체가 틀리게 나와 있는 경우도 있더군요. 그래서 만들어 봤습니다. 만들면서도 대충 하지 뭐 했는데 의외로 이상한 부분에서 발목이 잡히길래 기록도 할 겸 정리를 시작해 봅니다.

가장 중요한 건 전 Github 이 아닌 GitLab 을 쓰기 때문입니다.

# Jenkins 의 설치

Jenkins 는 설치 자체가 어렵지 않습니다. 이런저런 설치 옵션이 있지만, 귀찮고 내 서버는 아까우니까 docker 로 설치해 보겠습니다. 역시 가장 좋은 건 공식 가이드를 보는 것이죠.

공식 설치 가이드에 보면 이런저런 필요한 내용들이 나옵니다.^[Installing Jenkins, https://jenkins.io/doc/book/installing] 보통의 경우 큰 문제가 아닐테고, docker 니 더더욱 문제는 아니겠죠. 이 부분은 패스하고 바로 docker 로 갑니다. [Downloading and running Jenkins in Docker
](https://jenkins.io/doc/book/installing/#downloading-and-running-jenkins-in-docker) 부분을 보면, 권장하는 docker image 가 `jenkinsci/blueocean` 이라고 알려 줍니다. docker hub 에서 받을 수 있고요. 이 이미지가 LTS 를 지원하며, `production-ready` 라고 하네요. 그래서 이 버전을 설치 합니다.

위의 blueocean 이라는 이미지는 Blue Ocean 플러그인과 기능이 들어가 있어서 별도 설치가 필요 없다고 하는데, 사실 그 기능이 뭔지도 왜 필요한지는 모르겠습니다. 일단 문제는 없으므로 패스.

다음과 같이 docker pull 로 설치합니다.

```
$ docker pull jenkinsci/blueocean
Using default tag: latest
latest: Pulling from jenkinsci/blueocean
4fe2ade4980c: Pull complete
6fc58a8d4ae4: Pull complete
fe815adf554b: Pull complete
f76236630288: Pull complete
8039171c0e57: Pull complete
18d8e90ec574: Pull complete
7a2a4ab0865c: Pull complete
0e70b771820a: Pull complete
6e7e196001ad: Pull complete
c63c334888d6: Pull complete
1dd6901c0bea: Pull complete
ca93531eb098: Pull complete
4d0f9ddcdde1: Pull complete
9a912b50042f: Pull complete
eaa98ce77ad7: Pull complete
eb6a39a42842: Pull complete
Digest: sha256:ca216584e7689ea667853001b12b69b7a6cbe0dc44da44dc469647e56dca9e0e
Status: Downloaded newer image for jenkinsci/blueocean:latest
```

그 다음에 할일은 그냥 docker 실행입니다.

```
$ docker run \
>   -u root \
>   --rm \
>   -d \
>   -p 9999:8080 \
>   -p 50000:50000 \
>   -v $PWD/jenkins:/home/user \
>   -v /var/run/docker.sock:/var/run/docker.sock \
>   jenkinsci/blueocean
568b2d77e561fec89f3615a3e437e20b8d0d932ee19f68cfd2a3167ce35e3458
```

위의 `docker run` 옵션은 실제 제가 적용한 내용입니다.

사실 이 부분에서 설명할 것들이 좀 있긴 하지만, 그 내용은 [On macOS and Linux](https://jenkins.io/doc/book/installing/#on-macos-and-linux) 에 잘 설명이 되어 있습니다.

```
>   -p 9999:8080 \
>   -p 50000:50000 \
```

위의 두 가지만 설명이 필요할 듯 하네요. `-p` 는 port 를 지정하는 옵션이며, 앞의 숫자는 `docker host machine` 에서 외부로 노출될 port 번호이고, 뒤의 숫자는 `내부 컨테이너의 port` 번호 입니다.
즉, `http://host-machine-ip:9999` 로 접속하면, 내부 컨테이너의 `http://container:8080` 으로 연결이 되는 거죠. 아래의 50000 번 port 역시 동일합니다.

당연히 위에서 설정해주는 포트들은 well-known 포트들이 아니기도 하고, 잘 사용되지 않는 것들일 수 있어서 외부에서 접속이 되지 않는다면 본인의 방화벽 설정을 확인해 보셔야 합니다. 저 역시도 별도로 오픈한 포트들입니다.

8080, 50000 번 포트 외에도 다른 포트를 내부적으로 더 사용하는 것 같지만, 테스트 후 결과적으로 정리를 하는 지금은 저 두개만 연결해 주어도 정상적으로 동작, 플러그인 업데이트가 되는 것 까지 확인했습니다. 그 외 참조해야 할 포트를 알고 계시다면 알려 주세요.

## Jenkins 에 접속

```
$ docker ps -a
CONTAINER ID        IMAGE                 COMMAND                  CREATED             STATUS                       PORTS                                              NAMES
568b2d77e561        jenkinsci/blueocean   "/sbin/tini -- /usr/…"   4 seconds ago       Up 3 seconds                 0.0.0.0:50000->50000/tcp, 0.0.0.0:9999->8080/tcp   pedantic_keldysh
```

정상적으로 docker 가 실행중인 게 확인이 되었다면, 설치한 Jenkins 에 접속해 봅니다.
내가 Jenkins 를 설치한 서버의 9999, 다른 포트를 사용하셨다면 해당 포트를 지정하여 접속하면 됩니다.

그럼 아래와 같은 화면을 보게 됩니다.

![Jenkins 설치 후 초기 화면](../imgs/install-jenkins-with-gitlab/jenkins-screen01.png)

나머지 설치를 위해 Jenkins 가 구성되면서 만들어 놓은 `initialAdminPassword` 를 입력해야 하나 봅니다. 찾아 봅시다.

아무 생각 없이 해당 경로를 host machine 에서 찾아 보니 당연히 없습니다. docker 로 설치했으니까요. 그럼 어디에 있지 하고 위의 `(not sure where to find it?)` 을 따라가봤더니 단순히 jenkins logging 관련된 안내 페이지만 뜨네요.^[Jenkins Logging, https://wiki.jenkins.io/display/JENKINS/Logging]

Jenkins Installing 가이드를 다시 읽어 보면 아래에 잘 설명이 되어 있습니다. 해당 안내에 따라 다음과 같이 `docker logs [container-id]` 를 실행해 봅니다.

```
$ docker logs 568b2d77e561
Running from: /usr/share/jenkins/jenkins.war
...(생략)...
Oct 11, 2018 2:04:12 AM jenkins.install.SetupWizard init
INFO:

*************************************************************
*************************************************************
*************************************************************

Jenkins initial setup is required. An admin user has been created and a password generated.
Please use the following password to proceed to installation:

8c37fb6fb8ff421fb4654170338c7b1a ## 요기에 있네요.

This may also be found at: /var/jenkins_home/secrets/initialAdminPassword

*************************************************************
*************************************************************
*************************************************************

...(생략)...
--> setting agent port for jnlp
--> setting agent port for jnlp... done
```

혹은 직접 내가 들어가서 봐도 됩니다.

```
$ docker exec -it 568b2d77e561 /bin/bash
bash-4.4# cat /var/jenkins_home/secrets/initialAdminPassword
8c37fb6fb8ff421fb4654170338c7b1a
```

간단하죠? 해당 초기 암호를 아까 그 Jenkins 의 화면에 넣고 잠시 기다립니다. 간단한 admin 계정정보와 접속정보등을 입력하고 나면 다음과 같이 설치가 완료됩니다.

![Jenkins 설치 후 초기 모습](../imgs/install-jenkins-with-gitlab/jenkins-screen02.png)

`http://host-machine-ip:9999` 또는 설치 과정에서 정의한 도메인 등을 넣고 접속해서 확인이 가능합니다. 다시 말씀드리지만, 9999 포트 번호는 제가 직접 docker 설치 시에 변경한 거라 그런것일 뿐 기본 포트가 아님을 참고하세요.

여기까지 잘 따라왔다면 Jenkins 설치는 완료 입니다.

9999 번 포트는 그렇다 치고, 50000 번 포트는 어디에 쓰일까요? 한번 접속해 보세요. Jenkins 와 관련된 정보를 볼 수 있습니다.

# GitLab 의 연동

여기서 제가 왜 Github 을 놔두고 GitLab 을 연동하고자 했는지 설명이 필요하겠네요. 사실 Git 에 대해 처음 공부를 시작했을 때 아무 생각없이 저도 다른 사람들처럼 Github 에 계정만들고 이거저거 해보곤 했었습니다. 그런데, 최근에 개인적인 프로젝트를 하나 진행해보고자 했고, 미천한 실력으로 인해 소스 코드를 외부에 공개하기는 좀 미천한지라, Private Repository 를 만들고 싶었습니다. 그런데 Github 에서도 가능은 하지만 결정적으로 무료가 아니죠. 그래서 직접 설치/운영이 가능한 GitLab 을 선택했고, GitLab CE 를 제 개인 서버에 설치해 뒀습니다. 한번 설치해 놓고 보니 매우 유용합니다. GitLab CE 역시 docker 로 설치가 가능합니다.

여튼. 이래서 GitLab 을 연동하기로 했습니다.

## GitLab Jenkins Integration

제가 찾아본 문서에 의하면 Jenkins 는 GitLab-EE 버전에만 지원한다... 라고 알고 있었는데 통합이 되어 더 쉽게 사용할 수 있었던 거군요! 정리하려고 다시 찾아본 문서에서 지금 봤네요! ~~젠장~~ 사실 안되는 줄 알고 방법을 찾다가 많은 삽질을 했었는데.. 꼼꼼히 좀 읽어 볼 걸 그랬습니다.^[GitLab Jenkins Integration, https://about.GitLab.com/features/jenkins/]

너무 대충 읽었더니 몇 시간 삽질로 돌아 오네요. 원문은 아래와 같으니 참고 하시길 바랍니다. 어쨌든 CE 에서도 되긴 되나 봅니다. 안된다고 판단했던 문서가 있었는데, 그 문서를 못찾겠네요.

원문.
Easily and quickly configurable: **Jenkins is easily integrated with GitLab Enterprise Edition**, right from your project's integrations settings. Once you've enabled the service to configure GitLab's authentication with your Jenkins server, and Jenkins knows how to interact with GitLab, it's ready to use, out-of-the-box.

## Self-hosted GitLab 에 연동하기

즉, GitLab-CE 버전을 의미합니다. 결과를 우선 말씀드리자면, 제가 직접 서버에 설치한 GitLab-CE 와 Jenkins 는 서로 연동할 수 없었습니다. 버전에 따른 지원의 문제는 아니고, Jenkins 와 GitLab 은 사실 같은 서버에 설치되어 있었는데, GitLab 에서 Webhook 을 설정할 때 Localhost 로 인식해 버리면서 등록자체가 안되더군요. 그래서 어쩔 수 없이 GitLab.com 으로 옮겨와 설치할 수 밖에 없었습니다.

갑자기 생각나서 GitLab-CE 에는 설치가 불편한가 봤더니 그건 또 아니네요. GitLab.com 에서 연동한 메뉴가 똑같이 존재 하네요. 하지만 단순 테스트 용도로 만드는 Jenkins 를 인스턴스를 하나 더 띄울만한 돈이 없어서 이 부분은 패스 합니다.

## GitLab.com 에 연동하기

Github 도 아니다 보니 Webhook 등록도 아니고, 인증 자체를 붙이는데 고생을 많이 했습니다. 다시 말씀드리지만 몇몇 글들은 있으나 그간 GitLab 이 업데이트가 많이 되서 메뉴들이 많이 옮겨 갔네요. 그래서 다시 이렇게 정리중입니다.

실제 연동하기 전에 이거저거 준비해야 할 과정들이 좀 있습니다. 한번 살펴 보시죠.

### 연동에 앞선 준비과정

우선 먼저 [Jenkins Ci service](https://docs.gitlab.com/ee/integration/jenkins.html) 라는 문서를 보겠습니다.

여기서 요구사항을 보면 2개의 Plugin 설치와 접근 권한 및 계정을 요구 합니다.

- Jenkins GitLab Plugin
- Jenkins Git Plugin
- Git clone access for Jenkins from the GitLab repository
- GitLab API access to report build status

Plugin 은 Jenkins 에서 설치하면 되고, 나머지 두 개의 항목들을 살펴 보겠습니다.

`- Git clone access for Jenkins from the GitLab repository`

- Jenkins 가 설치된 서버에서 연동하고자 하는 GitLab repository 에 clone 할 수 있는 권한이 필요 합니다.
- SSH Key 의 등록이나 기타 등등의 일반적인 방법을 통해 `git clone` 이 가능하도록 만들어 둬야 합니다.

`- GitLab API access to report build status`

- build status 를 report 하기 위해 GitLab API 접근이 가능해야 합니다.
- 별도의 Access Token 을 받으면 됩니다.

### Jenkins Plugin 설치

Jenkins 에 접속한 후 아래의 경로로 갑니다.

**Manage Jenkins > Manage Plugins > Available**

전 이미 설치를 완료한 상태라 별 내용이 없지만, git 으로 검색하신 후 각각의 Plugin 을 설치하시면 됩니다.

- Git plugin
- GitLab Plugin

![이미 설치한 후의 결과화면](../imgs/install-jenkins-with-gitlab/jenkins-plugin01.png)

### GitLab API Access Token 받기

GitLab 에 자신의 계정으로 접속을 한 후 아래의 경로로 갑니다.

**오른쪽 상단의 본인 계정 클릭 > Settings > 왼쪽 메뉴 중 Access Tokens`**

![GitLab 의 API Token 발급](../imgs/install-jenkins-with-gitlab/gitlab-api-token.png)

적당한 권한(=Scopes)을 선택하신 후 아래 생성 버튼을 누르면 화면 상단에 해당 Token 이 보이게 되는데요, 주의하실 점이 하나 있습니다. 만드신 후에 아시겠지만 다음과 같은 경고 메시지가 Token 아래에 표시 됩니다.

**Make sure you save it - you won't be able to access it again.**

말 그대로 입니다. Token 을 발급 받은 후 다른 화면으로 이동하는 순간 해당 Token 더이상 볼 수가 없습니다. 어디에 반드시 적어 두세요. 저같은 경우는 화면은 그대로 유지해 두고 GitLab.com 새창을 하나 더 띄워서 나머지 작업을 진행했습니다. 여차하면 그냥 다시 발급받아도 되지만요.

여기까지 따라 오시는데 문제가 없었다면, 준비 과정은 모두 끝입니다. :)

## Jenkins 에서 GitLab 정보 등록하기

Jenkins 에서 GitLab 계정과 관련한 정보를 등록해줘야 합니다. 등록하기에 앞서 필요한 Credentials 을 등록합니다.

### GitLab API Token 등록하기

아래의 메뉴로 다시 이동합니다.

**Jenkins > Credentials > Stores scoped to Jenkins**

![Jenkins 의 Credentials](../imgs/install-jenkins-with-gitlab/jenkins-credentials01.png)

위에서 global 을 눌러 나온 화면에서 왼쪽 메뉴의 Add Credentials 를 선택합니다.

![Jenkins 의 Credentials](../imgs/install-jenkins-with-gitlab/jenkins-credentials02.png)

그리고 아래 처럼 Kind 에서 GitLab API token 을 선택한 후 입력해 주면 됩니다.

![Jenkins 의 Credentials](../imgs/install-jenkins-with-gitlab/jenkins-credentials03.png)

전 이미 등록해 있어서 이미 등록이 되어 있는 이름이라고 알람이 뜨네요. 


### GitLab 접근 가능한 SSH Key 등록하기

Add Credentials 에서 이번엔 SSH Username with private key 를 선택하여 등록합니다. 과정은 위와 같고 GitLab 에 접근 가능한 SSH Private Key(id_rsa) 를 등록합니다. (id_rsa.pub 와 같은 Public Key 가 아닙니다.)

크게 다른 건 없기에 이 부분의 스크린샷은 생략하겠습니다.

### GitLab Plugin 설정하기

아래의 메뉴로 다시 이동합니다.

**Jenkins > Manage Jenkins > Configure System > Gitlab**

아래의 내용으로 채워줍니다. 똑같이 적어도 문제 없습니다. 직접 설치한 CE 버전 혹은 별도의 gitlab 자체 구축으로 인한 URL 이 있다면 해당 URL 을 적어도 됩니다.

![Jenkins 의 Gitlab Plugin 연동 부분](../imgs/install-jenkins-with-gitlab/jenkins-gitlab-conf.png)

`Credentials` 부분에선 위에서 등록한 API 를 선택해 주시면 됩니다. 그 뒤에 `Test Connection` 버튼으로 정상 동작을 확인, `Success` 가 뜨면 연동이 된 겁니다.

이제 Jenkins 를 사용하기 위한 실제 연동이 완료 되었습니다. 이제 Jenkins 를 써봅시다. :)

## Jekins Project 및 Job 생성하기

먼저 다시 Jenkins 의 아래 메뉴로 가서 새로운 프로젝트를 만듭니다.

**Jenkins > New Item > 프로젝트 이름 입력 과 `Freestyle project` 를 선택**

![Jenkins 의 Project 생성하기](../imgs/install-jenkins-with-gitlab/jenkins-project01.png)

그 뒤 나오는 화면에서 다음과 같이 입력해 줍니다.
`General` 부분에서 `GitLab Connection` 에 `gitlab.com` 으로 설정, 아마 자동 선택되어 있을 겁니다.

![Jenkins 의 Project 설정하기](../imgs/install-jenkins-with-gitlab/jenkins-project-general.png)

`Source Code Management` 부분에서 `Git` 을 선택, 내가 사용하고자 하는 Gitlab/repository 주소를 적어 줍니다. 이 주소는 `git clone` 할 때 쓰는 주소와 동일해야 합니다. `git` 으로 시작하는 주소를 사용하라고 가이드에 나와 있으나, 저같은 경우는 이상하게 인증이 되지 않아서 `https` 주소로 시작하는 git repo url 과 `gitlab.com` 의 Credential, 즉 ID/PW 를 다시 등록했습니다. 그 뒤 인증이 가능하더군요. 다른 방법도 가능할 듯 보이나 여기까지 문제 없기에 넘어 가겠습니다.

인증정보만 정확히 입력하시면 큰 문제는 발생하지 않는 것 같았습니다. test 로 만들어서 다시 연동하기는 귀찮으니 스크린샷은 패스하겠습니다. 대신 상세 내용은 다음과 같이 입력하면 됩니다.

- Repository URL : `https://gitlab.com/{gitlab_id}/{repo_name}`
- Credentials : Add > Jenkins > Kind : Username with password 로 GitLab.com 계정 등록 후 선택
- Name : `origin`
- Refspec : `+refs/heads/*:refs/remotes/origin/* +refs/merge-requests/*/head:refs/remotes/origin/merge-requests/*`

보다 상세한 자료가 필요하시다면, 해당 plugin repo 의 [Git configuration](https://github.com/jenkinsci/gitlab-plugin#git-configuration) 부분을 참고 하시면 될 것 같습니다.

`Build Triggers` 에서는 다음과 같이 설정합니다. 자세히 보시면 webhook 주소도 보입니다. 해당 주소를 나중에 GitLab 에서 다시 등록해 주셔도 되고, 혹은 GitLab 자체에서 지원하는 Project Serivce 를 사용해도 됩니다. 이건 아래에서 다시 설명하겠습니다.

![Jenkins 의 Project 설정하기](../imgs/install-jenkins-with-gitlab/jenkins-project-built-triggers.png)

그 다음의 `Build Environment` 와 `Build` 에서는 딱히 설정할 것은 없고요, 그 아래의 `Post-bild Actions` 에서 다음과 같이 `Publish build status to GitLab` 를 선택합니다.

![Jenkins 의 Project 설정하기](../imgs/install-jenkins-with-gitlab/jenkins-project-post-build.png)

그리곤 Save 혹은 Apply 하시면 Jenkins 설정은 모두 끝입니다.

## GitLab 에 Jenkins Integrations 하기

이제 마지막 단계 입니다. 다시 GitLab.com 으로 돌아와서 아래의 경로로 이동합니다.

**Jenkins 에 연동할 Project > 연동할 Repository > 왼쪽 메뉴 중 Settings > Integrations**

![GitLab 의 메뉴 참조](../imgs/install-jenkins-with-gitlab/gitlab-menus.png)

아까 위에서 Jenkins webhook 주소를 직접 연동하려면 `Integrations` 에 직접 해당 webhook url 을 입력하시면 됩니다.

![GitLab 의 Integrations](../imgs/install-jenkins-with-gitlab/gitlab-webhook.png)

하지만 스크롤을 조금 더 내려보시면, `Project services` 라고 이미 잘 통합된 몇몇 applications 를 위한 서비스들이 보입니다. 이 중 `Jenkins CI` 를 선택하시면 됩니다.

![GitLab 의 Project Services](../imgs/install-jenkins-with-gitlab/gitlab-project-services.png)

그리고, 각 항목에 맞는 Jenkins 서버 주소와 만들었던 Project 이름, Jenkins 접속 계정 정보 등을 입력 후 `Testsettings and save changes` 를 눌러 확인해 보시면 됩니다.

# 최종 확인

GitLab 과 Jenkins 연동을 위한 모든 작업이 끝났습니다. 이제 GitLab 에서 Jenkins 와 연동한 Repo 를 수정 후 `git push` 를 해보면 Jenkins 에서 해당 작업에 대한 Job/Build 가 진행되는 것을 확인할 수 있습니다.

위와 같이 테스트 환경을 구축해 보았습니다. 다음엔 실제 Jenkins 로 CI/CD 와 관련된 어떤 작업을 할 수 있을지 알아봐야 할 것 같네요.

그 어떤 자료보다 jenkinsci/gitlab-plugin 의 repo/wiki 에 가보시면 Setup Example 가 무척 잘 나와 있습니다. 저도 그 자료를 따라 설정을 한 것이고요.

https://github.com/jenkinsci/gitlab-plugin/wiki/Setup-Example

하다가 막히는 경우, 위의 주소를 참고해 보시기 바랍니다. 그 외 GitLab 에서도 직접 작성한 자료가 있긴 하지만 내용이 조금 부실한 편입니다.^[Jenkins CI service, https://docs.gitlab.com/ee/integration/jenkins.html] Plugin repo/wiki 를 따라해 보세요.

정말 마지막으로, 모든 GitLab/Jenkins 연동 자료는 모두 영문입니다. 그에 비해 Jenkins 는 브라우저의 언어 설정을 따라 중구난방으로 어디는 한글, 어디는 영어로 나와서 가이드를 따라하는게 오히려 힘들 정도 입니다. 이 경우 `Locale` 이라는 Plugin 을 설치하여 강제로 언어 설정을 `en-US` 로 맞춰서 진행하는게 정신건강에 이롭습니다. 자세한 방법은 이미 잘 나와 있는 곳^[How force Jenkins to show UI always in English?, https://superuser.com/questions/879392/how-force-jenkins-to-show-ui-always-in-english]이 있으니 해당 자료를 참고 하시면 될 것 같습니다.
