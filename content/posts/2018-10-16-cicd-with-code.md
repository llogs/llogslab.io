---
title: "CI/CD 뒷북 때리기 - Part2.적용편"
date: 2018-10-16T01:29:22+09:00
tags: ["jenkins", "pipeline", "ci/cd"]
categories: ["devops"]
---

지난 포스팅을 통해 대충 CI/CD 가 무엇인지 개념을 통해 알아봤습니다. 이제는 실체를 보고 싶은데 어떻게 해야 할까요? 비개발자인 저에겐 역시나 맨땅에 헤딩이죠.

## 첫 시작은 역시

누군가 잘 정리해 놓은 자료겠죠. 혹은 실제 도입 사례등을 살펴보면 매우 도움이 됩니다. 그래서 CI/CD 를 통해 해결하고자 하는 문제가 무엇인지 찾아볼 수 있고 효과 역시 실제 구축 전 파악할 수 있기 때문이죠.

그래서 자료를 좀 뒤져봤습니다.

[IT 전문서, 지속적 통합,배포는 처음이라](http://www.realhanbit.co.kr/books/230) 라는 훌륭한 제목의 책이 하나 나오더군요. 해당 책은 7명의 현업 개발자분들의 CI/CD 도입 사례를 서술한 한빛 미디어의 무료 웹북입니다. 무려 무료!

![한빛미디어의 지속적 통합,배포는 처음이라](../imgs/what-is-cicd/hanbit-cicd.png)

여기에 나온 내용들의 요약 정리가 도움이 된 듯 하여 막간 홍보를 해봅니다. 가서 보세요. 여기에 나온 정보들이 정리에 아주 유용할 것 같이 가져올 까 싶었지만 무료라고 해도 책으로 출판된 내용이라 저작권의 문제가 있을 듯 하여 나름대로의 정리를 별도로 해야 할 것 같습니다.

그치만 기회가 되면 가서 읽어 보세요. 아무것도 몰라도 내용이 잘 와닿습니다.

## 마이크로 서비스 디자인과 지속적인 통합

이 제목은 제가 지은 것이 아닙니다. 정확히 MS Azure 에 [마이크로 서비스 디자인: 지속적인 통합](https://docs.microsoft.com/ko-kr/azure/architecture/microservices/ci-cd) 이라는 제목으로 올라온 글이 하나 있네요. 

제목에서도 알 수 있다시피, 지속적인 통합, 즉 CI 는 CD 의 개념을 내포하는 것 같기도 합니다. `통합` 의 의미를 결과적으로만 해석한다면, 프러덕션 레벨의 배포까지도 이야기 할 수 있을 것 같습니다.

여튼 이 글에선 마이크로 서비스 아키텍쳐, 즉 MSA 와 Kubernetes, Docker 와 같은 기술들을 사용해 설명합니다만, 개념은 몰라도 이해엔 문제가 없습니다. ~~CI/CD 도 모르는데 저걸 제가 어떻게 알겠어요..~~ 

MSA 를 채택하는 가장 큰 이유 중 하나는 더 빠른 릴리즈 주기, Release cycle 때문이라고 합니다. Cloud Native 한 환경에선 사용자의 급증,급감에도 대응해야 하고, 신규 기능의 빠른 출시가 곧 서비스의 경쟁력을 높이는데 도움이 되기도 하니 주기를 빠르고 안정적으로 가져가는 것 자체가 큰 장점이 되겠죠. 

쉽게 생각해도 느리고 무거운 어플리케이션들은 배포 주기도 느리고, 신기술의 반영 등의 업데이트도 느리고 패치도 느립니다. 우선순위에 따라 조정되기도 하지만, 아키텍처와도 관련이 없진 않겠죠. 이런면에선 MSA 가 좋은 장점을 가지고 있나 봅니다. 물론 단점도 분명 있는 것 같습니다. 이건 나중에 다시 정리를 해보는 것으로 하고 일단 패스하겠습니다.

### Monoliths and Microservices

'and' 가 아닌 'vs.' 가 붙었어야 할 것 같은 제목이네요. 빠른 배포 주기와 CI/CD 의 필요성을 이야기 하려다 보니 여기까지 오네요. MSA 에 보다 자세히 정리하겠지만 보통의 경우 위와 같이 Monoliths and Microservices 의 형태로 많이들 구분 짓습니다.

그 유명하고도 유명한 마틴 파울러 아저씨가 해당 글에 대해 정리한 글이 있습니다. 무려 2014년도입니다. [Microservices, a definition of this new architectural term](https://martinfowler.com/articles/microservices.html) 이라는 글인데요, 고맙게도 누군가 번역을 해주었습니다. 번역은 [Channy's blog](http://channy.creation.net/articles/microservices-by-james_lewes-martin_fowler) 에 가서 보실 수 있습니다.

간단하게 비교해 보자면,

#### 모놀리식(Monolithic) 스타일

- 하나의 큰 덩어리 단위로 구축
- 엔터프라이즈 레벨의 어플리케이션은 종종 세 가지 주요 부분으로 구성
  - Frontend
  - DB
  - Backend
- 보통의 Backend 쪽은 하나의 논리적인 실행 파일
- 시스템에 대한 변경이나 서버 측 어플레키여신이 변경되면 완전히 새로운 버전을 컴파일 하여 배포해야 함
- 요청을 처리하는 로직은 모두 단일프로세스로 수행
- 개발자 컴퓨터, 로컬에서 실행 및 테스트를 진행
- 이러한 테스트 완료 후 실 서버로 배포 가능한지 확인하는 배포 파이프라인을 사용
- 일반적으로 로드밸런서 뒤에 여러 (서버) 인스턴스에서 실행하고 확장이 가능한 구조
- 잘 동작하지만, 서서히 문제가 발생
- 모놀리식 구조의 어플리케이션을 클라우드에 점점 많이 배포하는 경우가 특히 그러함
- 특히 배포 주기에 문제
- 응용 프로그램 내 작은 부분에 대한 변경이 있더라도, 모놀리식 방식은 모두 다시 빌드하여 배포해야 함
- 시간이 지남에 따라 모듈 구조를 문제 없이 유지하는 것은 점점 어려워짐
- 각 모듈의 변경 사항을 그 모듈에만 한정하는 것도 힘들어 짐
- 서버 확장성도 자원을 더 필요로 하는 부분만 아니라 전체 어플리케이션의 규모에 따라 더 필요해짐

인데 반해,

#### 마이크로서비스(Microservices) 스타일

- 큰 어플리케이션을 여러 작은 서비스의 조합으로 구축
- 각 서비스는 독립적으로 배치 가능하고 확장 가능함
- 또한 서로 다른 프로그래밍 언어로 개발된 다른 서비스임에도 불구하고 명확한 모듈상의 경계를 가짐
- 각 서비스는 그것을 만든 팀들이 직접 관리
- 서비스를 통한 컴포넌트화

라고 할 수 있겠습니다.

그림으로 보면 차이는 더 극명하게 드러납니다.

![Monolith 와 Microservice](../imgs/what-is-cicd/mf-sketch.png)

모놀리식 기반의 어플리케이션은 하나의 단일 프로세스내에 모든 걸 때려 넣어야 하는데 비해 마이크로서비스는 기능별로 분리된 서비스로 구성이 가능합니다. 

모놀리식의 서버 확장(scales)은 같은 기능을 가진 동일한 서버를 여러대 만드는 반면, 마이크로서비스의 경우 필요한 서비스에 따라 각각 여러대의 서버로 서비스를 분산하여 구성할 수 있습니다.

데이터베이스도 비슷한 형태로 구성이 가능합니다. Single Database 와 Application Database 라고 볼 수 있겠죠.

![Single DB vs. Application DB](../imgs/what-is-cicd/ms-decentralised-data.png)

일단 MSA 에 대한 정리는 여기까지 하고, 이와 같이 빠른 배포 주기를 가져가기 위해선 마이크로 서비스 아키텍처의 도입이 더 효과적이다라는 부분만 이해되면 됩니다.

### 배포 환경 비교

다시 본론으로 돌아와서, MS 의 자료를 계속 보겠습니다. 찾아보니 더 깔끔하게 비교한 이미지가 있네요.
모놀리식과 마이크로 서비스에서의 릴리즈 비교 입니다.

![MSA vs Monolithic releaes](../imgs/what-is-cicd/cicd-monolith.png)

모놀리식 방식에선 코드를 하나로 모아 단일 빌드를 만들 때 하나의 부분 코드라도 문제가 될 경우 모든 코드를 롤백해야만 합니다. 배포나 릴리즈는 뒤로 밀릴 수 밖에 없는데요, 마이크로 서비스 같은 경우는 이와 다르게 해당 서비스의 부분만 문제가 될 뿐 나머지 코드의 배포엔 문제가 없게 되는 거죠.

즉, 이런 구조를 가져가게 됨으로서 지속적인 통합과 지속적인 전달 및 배포가 가능해지게 되는 것으로 보입니다. 아래는 마이크로소프트 내에 있는 본문을 그대로 가져와 봤습니다.

* 지속적인 통합은 주 분기의 코드가 항상 프로덕션 수준이 되도록 자동화된 빌드 및 테스트 프로세스를 사용하여 코드 변경 내용이 주 분기로 자주 병합됨을 의미합니다.

* 지속적인 업데이트는 CI 프로세스를 전달하는 코드 변경 내용이 프로덕션 환경과 유사한 환경으로 자동으로 게시되는 것을 의미 합니다. 라이브 프로덕션 환경으로 배포에는 수동 승인이 필요할 수 있지만 그렇지 않은 경우 자동화됩니다. 목표는 코드가 항상 프로덕션 환경으로 배포될 준비가 되어야 하는 것입니다.

* 지속적인 배포는 CI/CD 프로세스를 전달하는 코드 변경 내용이 프로덕션 환경으로 자동으로 배포되는 것을 의미합니다.

## 개발자가 되어 봅시다.

아무래도 전 개발자가 아니니 오히려 제 스스로 기존 개발 방식(?)을 만들어 사용하는 가정을 해본다면, 더 쉽게 문제점이 보일 수도 있겠네요.

### 개발환경 꾸며보기

할줄 아는 건 파이썬이니 파이썬 버전과 아마도 그 버전에 맞는 가상환경을 로컬에 만들 겁니다. 그리고 단순한 서비스임을 가정해 Alpha, Beta, Stage 등의 나눠진 단계들은 없고 바로 Production 으로 배포하도록 한다고 해봅시다. 그럼 필요한 건 개발자 노트북과 서비스 서버뿐일 겁니다.

- 개발자 로컬 환경
  - 개발 환경 셋업, 아마도 가상환경
  - 개발의 시작, 코드 작성
  - 유닛 테스트 코드 작성
  - 가상환경으로 만든 라이브러리 혹은 패키징 정리 등의 정리
  - 서비스 실행
  - 실행 결과 확인
    - 문제가 생긴다면 다시 개발의 단계로 되돌아가 문제가 해결될 때 까지 반복
    - 문제가 없다면 git 에 코드 merge
  - 새로운 코드를 개발 혹은 유지보수
- 서비스 서버 환경
  - 기존 코드 백업
  - 소스 코드가 저장된 git 을 가져와 배포
  - 만약 문제가 생긴다면, 서비스 장애등과 같은 상황을 직면
  - 롤백을 진행
  - 다시 배포 일정 및 코드 검증을 진행, 회고

딱 보며 큰 문제는 없어 보입니다. 아무래도 서비스 자체가 중요도가 낮고 큰 규모가 아니다 보니 혼자 쿵짝쿵짝 하면 되는 거니까요. 다만 실제 대규모 서비스 환경의 경우 나혼자 개발하지도 않고, 여러가지 서비스가 유기적으로 물려 있으며 이 서비스의 구조가 위에서 설명한 모놀리식 스타일이라면 내 코드 한번 반영하려 줄 서서 기다려야 할지도 모르겠네요. 물론 이건 개발자로서 경험이 없는 제 기준의 시나리오 입니다. 아마도 이런 경우를 회피하려 다양한 소프트웨어 개발 방법론이 존재할 겁니다.

참고로, 소름돋게도 위의 모든 일련의 작업들은 수.동. 입니다. 굳이 자동화할 단계까지 갈 필요도 없을 것 같습니다. 그러나 엔터프라이즈 레벨의 대규모 서비스라면, 위와 같이 사람이 코드를 백업하고, 수십 수백대의 서버에 같은 코드를 내려 받고, 문제가 생기지 않도록 수동으로 롤링 업데이트를 하는 건 잠깐 생각해봐도 쉽지 않을 것 같습니다. 이런 이유로 자연스럽게 클라우드 시대와 맞물려 마이크로 서비스 아키텍처의 시대로 가나 봅니다.

그럼 이제 CI/CD 의 실체를 살펴 봅시다!

## 세상 많은 CI/CD 서비스

이 세상엔 어떠한 CI/CD 서비스가 있을까요? 찾아 봅시다.

- Jenkins
- Travis CI
- Circle CI
- GitLab
- Spinnaker
- GoCD

위는 제가 클라우드 세미나 쫓아다니면서 한번이라도 들어본 이름들만 나열해 봤습니다. 순서는 없고요. 그 외에도 무진장 많습니다. 찾아본 글들 중엔 하나의 글에서 무려 51개의 CI/CD 툴을 소개하기도 합니다.^[Top Continuous Integration Tools: 51 Tools to Streamline Your Development Process, Boost Quality, and Enhance Accuracy, https://stackify.com/top-continuous-integration-tools/]

각각을 아주 간략히 살펴 봅시다. 써보지도 않고 뭘 알 수 있겠냐만은, 위의 글을 기준으로 간략히만 정리해 봅니다.

### Jenkins

https://jenkins.io/

가장 첫번째로 Jenkins 입니다. 가장 널리, 그리고 웬만하면 다들 Jenkins를 사용하는 것 같아요. 저도 테스트를 위해 Jenkins를 설치해 뒀습니다. 게다가 친절하게도 설치 과정을 글로 정리까지 해뒀습니다. [CI/CD 뒷북 때리기 - Part1.개념편](../2018-10-16-what-is-cicd) 을 보시면 됩니다. 이걸로 무엇을 할지는 다시 아래에서 살펴보도록 하겠습니다.

아래와 같이 공통적으로 서술되는 Key Features 와 Cost 부분은 굳이 번역하지 않아도 될 것으로 생각합니다. 굳이 번역은 가능하나 실제 의미전달이 잘 되지도 않고 본문 자체가 더 명확하게 의미 전달이 되는 것 같기 때문입니다. CI/CD 의 개별 서비스들에 대해 마스터 하자는 것이 아니므로, 이정도로만 알고 넘어가도 좋을 것 같기도 하고요.

여튼 Jenkins는 넘버 원 CI/CD 툴 중에 하나며, 보통 building projects, running tests, bug detection, code analysis, and project deployment 에 사용된다고 합니다.

Key Features:

- Jenkins works as a standalone CI server, or you can turn it into a continuous delivery platform for virtually any of your projects
- Pre-built packages for Unix, Windows, and OS X ensures an easy installation process
- A web interface that can be used to quickly configure your server
- Custom plugins for build and source code management, administrative tasks, user interface, and platforms
- Deployable across a network of machines, improving performance of builds and tests
- Large community with leading software brands involved in development

Cost: Open Source

### Travis CI

https://travis-ci.org/

Travis 는 CI 플랫폼중에 하나로서 software testing 과 deployment of application 의 자동화를 도와 준다고 합니다. GitHub 과 연동이 가능하며, 내 코드를 실제로 테스트 해볼 수 있습니다. 시장에서의 CI 리딩 툴 중 하나라고 합니다. ~~어째 많이 들어 봤다 했어..~~

Key Features:

- Free for public open source projected on GitHub
- As simple as signing up, adding a project, and you can begin testing
- Bilingual support so your code runs smooth across all versions
- Automated pull request verification
- Works with Email, Slack, HipChat and others for easy notifications
- Extended API and CMD tools for custom management

Cost: Free for open repositories, Enterprise for private

### Circle CI

https://circleci.com/

CircleCI 도 많이 들어본 것중에 하나입니다. 다만 써보지 못한 것들중에 하나이기도 할 뿐..

Key Features:

- Create an account, add a project, and start building. The platform can grab custom settings from your code directly
- Custom integration with Maven, Gradle, and other top-notch build tools
- Using Django, Nose, RSpec, and countless others, CircleCI will do a stellar job at testing your code, while you’re busy shipping your next product
- Flawless integration with AWS, Heroku, Google Cloud, and others
- CircleCI automatically uses language-specific tools like rvm and virtualenv to ensure dependencies are installed into an isolated environment

Cost: Free, Premium Starting at $50

### GitLab

https://gitlab.com/

GitHub 과 비슷한 code management platform 입니다. 쉽게 말해 repo 서비스를 제공합니다. GitHub 이 마이크로소프트에 인수되면서 부터 GitLab 이 급성장(?)하는 중입니다. 라고 합니다. 많이들 넘어가기도 하고 물들어 올 때 노 젓는다고 인수가 보도되자 마자 그날 밤부터 GitLab 은 어떻게 GitHub 에서 옮겨 올 수 있을지를 안내하는 가이드를 뿌리며 대응까지 했었습니다.

그 흔적은 [GitLab 의 Twitter](https://twitter.com/gitlab/status/1004143715844124673) 에서 보실 수 있습니다. [2018/06/04, Microsoft to acquire GitHub for $7.5 billion](https://news.microsoft.com/2018/06/04/microsoft-to-acquire-github-for-7-5-billion/) 이라는 기사가 나온 후 단 3일 만에 엄청난 수의 프로젝트가 GitHub 에서 GitLab 으로 옮겨갔습니다.

![GitLab 의 Twitter](../imgs/what-is-cicd/gitlab-twitter.png)

혹시나 옮겨가실 분들은 [Import your project from GitHub to GitLab](https://docs.gitlab.com/ee/user/project/import/github.html) 가이드를 참고 하세요. 생각보다 GitLab 이 좋습니다. 물오른 1등보단 맹추격하는 2등의 서비스가 항상 더 좋은 것 같아요. 사설이 길었네요.

code management platform 이지만 CI/CD 가 아주 잘 내재화(?) 되어 있습니다. 그래서 CI/CD Tool list 에서도 잘 보이는 것 같고요.

참고로 저도 GitHub 은 repo star 만 찍으러 다니고, 실제 코드는 모두 GitLab 에서 관리하고 있습니다. GitLab 은 게다가 self-hosting 이 가능한 설치형 GitLab-CE 도 지원합니다.

거기에 issue management, code views, continuous integration and deployment, all within a single dashboard 도 제공되죠. Wiki 도 있어요.

Key Features:

- Integrated directly into the GitLab workflow
- Add additional machines to scale your tests for performance
- CMD build scripts allow you to program them in any language
- Custom version tests to check branches individually
- Manual deployment, and effortless rollback capabilities

Cost: Free for Community Edition, Enterprise Starting at $16 per user

설치형으로도 가능하다 보니 GitLab 자료를 찾다보면, 사내에서 GitHub Enterprise 버전 대신 GitLab 을 쓰는 곳들도 많아 보였습니다.

### Spinnaker

https://www.spinnaker.io/

간간히 Circle/Travis 와 함께 들려오는 걸 보면 요즘 핫(?)한 CI/CD 툴 중에 하나로 보입니다. ~~다시 한번 말하지만 전 개발자가 아니라 그냥 그런 느낌이다... 라는 걸 말씀드리는 거에요. 오해 없으시기를..~~

Spinnaker 는 오픈 소스이며, 멀티 클라우드를 지원한다고 합니다. 글만 본다면 아주 좋은 평가를 받고 있는 듯 합니다.

Key Features: 

- Integrates with a variety of cloud providers
- Integrates with Jenkins and Travis
- Support for messaging through Slack, Hipchat, email, and more
- Cluster management
- Deployment management

Cost: Open Source

### GoCD

https://www.gocd.org/

느낌이 강하게 온게 Golang 과 관련된 플랫폼인줄 알았습니다. 근데 아닌가 봐요. Go 만 붙으면 죄다 Golang 관련된 툴인 줄만 알아서..

Key Features:

- Value Stream Map to monitor changes as they are being pushed in commits
- Custom triggers to manually save functional versions of your apps
- Understand why your builds break with an extensive test reporting pipeline
- Analyze broken pipelines through build comparison; files, and commits
- Custom template system for configuration reuse

Cost: Open Source

### 그 외

에도 무척 많은 것 같고, 저도 단순 써보지 못한 것들이 거의 다여서 이럴 땐 이것을, 요런 환경엔 저 툴을 써야 한다는 잘 모르겠습니다. 사용하면서 문제를 해결하기 위해 하나씩 방법을 찾아가면서 적용해 가는 게 자기에게 제일 좋은 툴을 찾게 될 것 같네요.

그 외에 Public repo 에 한정하여 무료라는 점 등은 개인 개발자들이 유의해야 할 점으로도 보이네요. 혹시 잘 비교된 자료가 있으면 알려 주세요. 일부러 찾지 않은 이유는, 굳이 비교해도 와닿지 않을 것이기 때문이기도 합니다.

## 이제 써봅시다.

저는 앞서 말씀드린 바와 같이 Jenkins 를 이미 설치해 두었고, Jenkins 를 쓰면 안될 이유를 굳이 찾지 못했으므로 그대로 사용해 보도록 하겠습니다.

왜 Jenkins 인가? 라는 설명은 우아한 형제들의 기술 블로그에 [라이더스 개발팀 모바일에서 CI/CD 도입](http://woowabros.github.io/experience/2018/06/26/bros-cicd.html) 라는 제목으로 잘 정리되어 있습니다.

해당 글에서 제시한 장점만을 본다면 다음과 같습니다.

- 무료 사용
- 사용자 정의 옵션
- 방대한 양의 플로그인
- 다양한 적용사례 및 풍부한 레퍼런스
- Remote access API 제공

이제 뭔지 알았으니 진짜 써봅시다.

### Jenkins 사용해 보기

라고 제목은 정했는데 사실 뭐부터 해야 할지 모르겠습니다. 이미 테스트 환경을 만들어두긴 했고, 뭔가 돌긴 돕니다만.. 뭐가 뭔지 잘 모르겠습니다. 그래서 찾아 봤습니다.

의외로 ITWorld 에서 자료를 잘 정리해 놨습니다. 한번 쭈욱 읽어보고 따라해 보려 합니다. 테스트 관련한 코드는 카카오 챗봇을 하나 아주 간단히 만들어 뒀습니다. 해당 코드를 GitLab 에 올려보고, Jenkins 랑 붙여서 어떻게 되는 지 한번 알아 보겠습니다. GitLab 자체에도 CI/CD Pipeline 이 이미 정의되어 있고, 여기에 Webhook 과 같은 형태로 Jenkins 를 붙일 수 있습니다. 이거저거 해보죠. 안되면 지우면 되니까요.

ITWorld 의 자료는 [Jenkins란 무엇인가, CI(Continuous Integration) 서버의 이해](http://www.itworld.co.kr/news/107527) 라는 제목으로 정리되어 있습니다. 시간될 때 한번 읽어보는 게 좋겠지만, 전 이미 스터디 발표 2시간 전이라 시간이 없습니다. 스터디 원들이 이글을 보지 않았으면 좋겠습니다. ~~날 용서해요..~~

#### 일단 Code 를 GitLab 으로

사실 해당 코드는 제 GitLab-CE 에 올려 뒀기 때문에 해당 repo 를 우선 clone 받습니다. 그리고 GitLab.com 에 가서 새로운 프로젝트를 하나 만듭니다. README.md 를 initialize 하지 않으면 GitLab.com 은 친절하게 기존 repo 를 어떻게 가지고 올 수 있는지를 알려 줍니다.

다음과 같습니다.

1) 내 코드 clone 받아 오기

```
❯ git clone git@[my.gitlab.ce]:lhkim/mkrdag.bot.git
Cloning into 'mkrdag.bot'...
remote: Counting objects: 52, done.
remote: Compressing objects: 100% (47/47), done.
remote: Total 52 (delta 18), reused 0 (delta 0)
Receiving objects: 100% (52/52), 17.64 KiB | 2.94 MiB/s, done.
Resolving deltas: 100% (18/18), done.
```

2) 정상적으로 코드가 clone 되어 왔다면, 다음과 같이 remote origin 의 이름을 변경

```
❯ git remote rename origin old-origin
```

3) GitLab.com 에서 새롭게 만든 프로젝트의 repo 를 추가, 여기선 `krdag_bot_test`

```
❯ git remote add origin git@gitlab.com:llogs/krdag_bot_test.git
```

4) 기존 코드를 GitLab.com 으로 git push

```
❯ git push -u origin --all

Enumerating objects: 52, done.
Counting objects: 100% (52/52), done.
Delta compression using up to 4 threads
Compressing objects: 100% (29/29), done.
Writing objects: 100% (52/52), 17.64 KiB | 17.64 MiB/s, done.
Total 52 (delta 18), reused 52 (delta 18)
To gitlab.com:llogs/krdag_bot_test.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
```

5) tag 정보 역시 push

```
❯ git push -u origin --tags
Everything up-to-date
```

그리고 해당 repo 에 가보면 아래처럼 보입니다.

![GitLab.com 에서의 repo 첫 화면](../imgs/what-is-cicd/gitlab-repo.png)

Auto DevOps 라는 게 보이면서 미리 정의한 CI/CD 설정에 의해 자동으로 build, test, deploy 까지 해준다고 합니다. 사실 이게 위에 Gitlab 에서 이야기 드렸던 이미 잘 제공되어지는 GitLab.com 의 CI/CD 서비스 입니다. GitLab 에서는 `autodevops` 라고 부릅니다.

자세한 내용을 살펴보고 싶지만 우리에게 남은 시간이 별로 없어요. ~~사실 나에게 없어요...~~ 그래서 시간많은 사람들을 위해 일단 링크만 걸어 봅시다. [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) 라는 문서에서 관련 내용을 살펴볼 수 있어요. 누가 읽으면 저도 알려 주세요. 정말 많은 기능들이 있어서 나중에 한번 테스트 해보고 싶네요. 일단 Jenkins 에 집중합시다.

#### django 프로젝트와 Jenkins 연동하기

여기저기 찾아보면, 몇몇의 파이썬 패키지와 설정을 통해 자연스럽게 연동이 가능한가 봅니다. 다음은 필요해 보이는(?) 파이썬 라이브러리 입니다.

- [django-jenkins](https://github.com/kmmbvnr/django-jenkins)
  - django 프로젝트를 위한 jenkins 라이브러리
- [pep8](https://pypi.org/project/pep8/)
  - 파이썬 스타일 가이드 체커
- [pyflakes](https://pypi.org/project/pyflakes/)
  - 소스 코드 분석을 통한 에러 검출을 도와주는 라이브러리
- coverage

pep8 빼고는 다 처음 들어보는 것들이라 자세한 내용은 각각 찾아 보시면 될 것 같습니다. 일단 모두 설치해 줍니다.

그다음 필요한 건 Jenkins 의 plugin 들입니다. 아래 목록을 설치해 줍니다.

- CVS 와 관련된 건 알아서 설치해 주세요.
  - 전 Gitlab 이라 Gitlab 관련 plugin 을 설치했습니다.
- Violations
  - Violation Comments to GitLab 이란 것도 보이길래 눈치껏 설치
- Cobertura
  - [Cobertura Report](http://cobertura.github.io/cobertura/)를 Jenkins 에서 사용할 수 있게 해주는 plugin

